"use strict";

var __importDefault = void 0 && (void 0).__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", {
  value: true
});

var express_1 = __importDefault(require("express"));

var body_parser_1 = __importDefault(require("body-parser"));

var serve_static_1 = __importDefault(require("serve-static"));

var mongoose_1 = __importDefault(require("mongoose"));

var cors_1 = __importDefault(require("cors"));

var Web3 = require('web3');

var shopkeeperController = require("./controllers/Shopkeeper.controller.js");
/*   MONGO_HOSTNAME="213.32.95.79"
  MONGO_PORT="27017"
  MONGO_DB="213.32.95.79"
  MESSAGE_QUEUE="amqp://guest:guest@213.32.95.79:5672"
  HOSTNAME="127.0.0.1"
  ADRESSE="127.0.0.1:3002"
  PORT="3002"
  EUREKA_HOST="213.32.95.79"
  EUREKA_PORT="8761"
  DATABASE="213.32.95.79"
  DBPORT="27022" */


var _process$env = process.env,
    MONGO_HOSTNAME = _process$env.MONGO_HOSTNAME,
    MONGO_PORT = _process$env.MONGO_PORT,
    MONGO_DB = _process$env.MONGO_DB,
    MESSAGE_QUEUE = _process$env.MESSAGE_QUEUE;

var Eureka = require('eureka-js-client').Eureka;

var eureka = new Eureka({
  instance: {
    instanceId: 'MSFIDCARD',
    app: 'MSFIDCARD',
    hostName: process.env.HOSTNAME,
    ipAddr: process.env.ADRESSE,
    statusPageUrl: 'http://' + process.env.HOSTNAME + ':' + process.env.PORT,
    port: {
      '$': process.env.PORT,
      '@enabled': 'true'
    },
    vipAddress: 'gestionfidcard',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn'
    },
    registerWithEureka: true,
    fetchRegistry: true
  },
  eureka: {
    host: process.env.EUREKA_HOST,
    port: process.env.EUREKA_PORT,
    servicePath: '/eureka/apps/'
  }
});
eureka.logger.level('debug');
eureka.start(function (error) {
  console.log(error || 'complete');
});

var amqp = require('amqplib/callback_api');

amqp.connect(process.env.MESSAGE_QUEUE, function (error0, connection) {
  if (error0) {
    throw error0;
  }

  connection.createChannel(function (error1, channel) {
    if (error1) {
      throw error1;
    }

    var exchange = 'new_shopkeeper_exchange';
    channel.assertExchange(exchange, 'fanout', {
      durable: true,
      autoDelete: false
    });
    channel.assertQueue('new_shopkeeper1', {
      exclusive: false,
      durable: true,
      autoDelete: false
    }, function (error2, q) {
      if (error2) {
        throw error2;
      }

      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');
      channel.consume(q.queue, function (msg) {
        if (msg.content) {
          var user = JSON.parse(msg.content.toString());
          var shopkeeper = new Shopkeeper({
            brandName: user.brandName,
            logo: user.logo,
            _id: user.id
          });
          shopkeeperController.addRabbitMQ(shopkeeper); //var obj = ;

          console.log(" [x] %s", user);
          console.log(" [x] %s", shopkeeper);
        }
      }, {
        noAck: true
      });
    });
  });
});
var Database;
/* Instancier Express */

var app = express_1["default"]();
/* Middleware bodyParser pour parser le corps des requêtes en Json*/

app.use(body_parser_1["default"].json());
/* Middlware pour configurer le dossier des ressources statique*/

app.use(serve_static_1["default"]("public"));
/* Actvier CORS*/

app.use(cors_1["default"]());
app.use(body_parser_1["default"].urlencoded({
  extended: true
}));

var MongoClient = require('mongodb').MongoClient;

var Shopkeeper = require("./models/Shopkeeper.js");
/*const uri = "mongodb+srv://houssemmh:26935040@cluster0.woupd.mongodb.net/mscards?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true });
mongoose_1.default.connect(uri,(err)=>{
        if(err){ console.log(err); }
        else{ console.log("Mongo db connection sucess");}
});*/


var uri = "mongodb://" + process.env.DATABASE + ":" + process.env.DBPORT + "/mscards?retryWrites=true&w=majority";
var client = new MongoClient(uri, {
  useNewUrlParser: true
});
mongoose_1["default"].connect(uri, function (err) {
  if (err) {
    console.log(err);
  } else {
    console.log("Mongo db connection sucess");
  }
});

require("./routes/fidCard.routes")(app);

require("./routes/shopkeeper.routes")(app);
/* Démarrer le serveur*/


app.listen(3002, function () {
  console.log("Server Started on port %d", 3002);
});