const mongoose = require('mongoose');
const mongoosePaginate = require("mongoose-paginate-v2");

const FidelityCardSchema = mongoose.Schema(
  {
    code: { type: String },
    type: { type: Boolean },
    customer: {
      type: String,
      ref: 'Customer'
    },
    balance: { type: Number },
    walletAdress: { type: String },
    shopkeeper: {
      type: String,
      ref: 'Shopkeeper'
    }
  },
  { timestamps: true },
  { id: false }
);
FidelityCardSchema.set('toObject', { getters: true });
FidelityCardSchema.method("toJSON", function () {
  const { __v, _id, ...object } = this.toObject();
  object.id = _id;
  return object;
});

FidelityCardSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('FidelityCards', FidelityCardSchema);