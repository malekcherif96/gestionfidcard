const mongoose = require('mongoose');
    const HistoriqueSchema= mongoose.Schema(
        { 
          type: {type: String},
          orderId: {
            type: String,
            default: null
           },
          customer: {
            type:String,
            ref: 'Customer'
          },
          fidcard:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'FidelityCards'
          },
          id_Store: { type: String},
          shopkeeper: {
            type: String,
            ref: 'Shopkeeper'
          },
          undershop:{
            type: String,
            ref: 'underShop'
          },
          dateCreation: {type: Date},
          pointFid: {type: Number},
          shopkeeperSource: {
            type: String,
            ref: 'Shopkeeper'
          },
          customerSource: {
            type:String,
            ref: 'Customer'
          },
          fidcardSource:{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'FidelityCards'
          }
        },
        { timestamps: true },
        {id:false}
      );
      HistoriqueSchema.set('toObject', {getters: true});
      HistoriqueSchema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      });
 module.exports = mongoose.model('Historique', HistoriqueSchema);