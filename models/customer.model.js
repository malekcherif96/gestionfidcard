
const mongoose = require('mongoose')

const customerSchema = new mongoose.Schema({
    

    _id: {
        type: String
    },
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    phoneNumber: {
        type: String
    },
    cddress: {
        type: String
    },
    contry: {
        type: String
    },
    city: {
        type: String
    },
    birthDay: {
        type: Date
    },
    gender: {
        type: String
    },
    postalCode: {
        type: String
    },
    status: {
        type: String
    },
    imageName:{
        type: String,
        default:''
    },
    historiques: [
        { type:mongoose.Schema.Types.ObjectId, ref: 'Historique' }
    ],

    fidelityCards: [
        { type:mongoose.Schema.Types.ObjectId, ref: 'FidelityCards' }
    ]
  
},  { timestamps: true },
{id:false}
);

customerSchema.set('toObject', {getters: true});

customerSchema.method("toJSON", function() {
    const { __v, _id, ...object } = this.toObject();
    object.id = _id;
    return object;
  });
  



module.exports = mongoose.model('Customer',customerSchema)

