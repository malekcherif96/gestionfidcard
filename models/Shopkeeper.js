const mongoose = require('mongoose');
const ShopkeeperSchema= mongoose.Schema(
    {
        _id: {
          type: String
        },
        brandName: {type: String},
        logo: { type: String},
        fidelityCards: [
            { type: mongoose.Schema.Types.ObjectId , ref: 'FidelityCards' }
        ],
        activated:{ type: Boolean},
        tauxRemise:{ type: Number},
        tauxConversion:{ type: Number},
        taxIdentificationNumber:{ type: Number},
    },
    { timestamps: true },
    {id:false}
    );
    ShopkeeperSchema.set('toObject', {getters: true});
    ShopkeeperSchema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      });
 module.exports = mongoose.model('Shopkeeper', ShopkeeperSchema);