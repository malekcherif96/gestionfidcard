const mongoose = require('mongoose');
    const AccordFidcardSchema= mongoose.Schema(
        { 
          shopkeepers: [
            {type: String, ref: 'Shopkeeper'}
          ],
          dateCreation: {type: Date},
          dateExpiration: {type: Date},
          ceiling: {type: Number},
          type: { type: String },
          contractFile: [{ type: String }],
        },
        { timestamps: true },
        {id:false}
      );
/*       AccordFidcardSchema.set('toObject', {getters: true});
      AccordFidcardSchema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      }); */
 module.exports = mongoose.model('AccordFidcard', AccordFidcardSchema);