const mongoose = require('mongoose');
    const FidelityCardPersoSchema= mongoose.Schema(
        { 
          code: {type: String},
        
          customer: {
            type:String,
            ref: 'Customer'
          },
  
          nameCard: {type: String},
          imageCard:{type: String , /* require:true */},
          nameShop:{type:String, /* require:true */},
          format: {type: String, default:'EAN_8'},
          pass:{type: Boolean, default: false}
        },
        { timestamps: true },
        {id:false}
      );
      FidelityCardPersoSchema.set('toObject', {getters: true});
      FidelityCardPersoSchema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      });
 module.exports = mongoose.model('FidelityCardsPerso', FidelityCardPersoSchema);
