
var mongoose = require('mongoose');

const underShopSchema=  mongoose.Schema(
    {
      
        _id: {
            type: String
        },
        id_Shopkeeper: {type: String},
        firstName: { type: String},
        lastName: { type: String},
        phoneNumber: { type: Number},
        role: { type: String},
        id_Store: { type: String},
        storeName: { type: String},


      
    },
    { timestamps: true },
    {id:false}
    );
   

    underShopSchema.set('toObject', {getters: true});
   /*  underShopSchema.set('toJSON', {getters: true}); */
    underShopSchema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
      });
  
 module.exports = mongoose.model('underShop', underShopSchema);