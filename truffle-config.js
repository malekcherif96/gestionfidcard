const PrivateKeyProvider = require("@truffle/hdwallet-provider");
const privateKey = "c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3";
var mnemonic = "the only thing that can change your live your passion be proud"
const privateKeyProvider = new PrivateKeyProvider(privateKey, "http://127.0.0.1:8545/");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 8545,
      network_id: "*" ,// Match any network id
      
    },
    besuWallet: {
      provider: privateKeyProvider,
      network_id: "*",
      gasPrice:0,
      //defaultEtherBalance: 500,
      blockTime: 3,
      gas: "0x1ffffffffffffe"
    },
    develop: {
      port: 8545
    },
    
  },
  compilers: {
    solc: {
      version: "0.6.2",
    },
  }
};
