const contract = require('@truffle/contract');
const tokenTunisie_artifact = require('../build/contracts/TokenTunisie.json');
var TokenTunisie = contract(tokenTunisie_artifact);
const Web3 = require('web3')
var Accounts = require('web3-eth-accounts');
//var accounts = new Accounts('ws://localhost:8545');
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
module.exports = {
  start: function(callback) {
    var self = this;
    //var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));

    // Bootstrap the MetaCoin abstraction for Use.
    TokenTunisie.setProvider(web3.currentProvider);
  
    // Get the initial account balance so it can be displayed.
    web3.eth.getAccounts(function(err, accs) {
      if (err != null) {
        console.log("There was an error fetching your accounts.");
        return;
      }

      if (accs.length == 0) {
        console.log("Couldn't get any accounts! Make sure your Ethereum client is configured correctly.");
        return;
      }
      self.accounts = accs;
      self.account = self.accounts[2];
     

      callback(self.accounts);
    });
  }
  ,
  getPrivateKey: async function(acc){
    //var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    //var accounts = new Accounts('ws://localhost:8545');
    
    var l = await web3.eth.accounts.wallet;
    console.log("wallet finding ...",l.length);
    //console.log("wallet finding ...",aa);
    var x;
    for (let index = 0; index < l.length; index++) {
      console.log(l[index].address);
      console.log("***",acc);
      if(l[index].address == acc){
        console.log("private key found ***", l[index].privateKey)
        x = l[index].privateKey;
        //callback(x);
        return x;
      }
      console.log(index);
    }
    return '0x00'
   
    //console.log("end get PK");
  },
  getBalance: function(account , callback) {
    var self = this;
    TokenTunisie.setProvider(web3.currentProvider);
    console.log("app : ",account);
    var meta;
    
    TokenTunisie.deployed().then(function(instance) {
      meta = instance;
      
      return  meta.balanceOf(account);
    }).then(function(bal) {
      callback(bal);
    }).catch(function(e) {
      console.log(e);
      callback(false);
    });
  },
  mint: function(account ,amount, callback) {
    //var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    var self = this;
    TokenTunisie.setProvider(web3.currentProvider);
    console.log("app : ",account);
    var meta;
    
    TokenTunisie.deployed().then(function(instance) {
      meta = instance;
      //console.log(web3.utils.fromWei(amount));
      // ta =self.getPrivateKey(account);
      // console.log("--------",ta);
      return  self.getPrivateKey(account);
    }).then(function(privateKey) {
      console.log("success privateKey", privateKey);
      //callback("SUCCESS");
      //account=  "'"+account+"'";
      //console.log(account);
      var tx =web3.eth.abi.encodeFunctionCall({name: 'mint', type: 'function', inputs: [{type: 'uint256', name: 'amount'}]}, [amount]);  
      //console.log(tx);
      return web3.eth.accounts.signTransaction({ from : account,
        to: TokenTunisie.address ,data : tx, value: 0, gas: '0x1ffffffffffffe'},
        privateKey);
    }).then(function(transaction) {
      console.log("success signed Transaction");
      //callback("SUCCESS");
      return web3.eth.sendSignedTransaction(transaction.rawTransaction);
    }).then(function() {
      console.log("success Mint");
      callback(true);
    }).catch(function(e) {
      console.log(e);
      callback(false);
    });
  },
  burn: function(account ,amount, callback) {
   // var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    var self = this;
    TokenTunisie.setProvider(web3.currentProvider);
  
    var meta;
    
    TokenTunisie.deployed().then(function(instance) {
      meta = instance;
      //console.log(web3.utils.fromWei(amount));
      //account=  "'"+account+"'";
      console.log(account);
      return  self.getPrivateKey(account);
    }).then(function(privateKey) {
      console.log("success privateKey", privateKey);
      //callback("SUCCESS");
      //account=  "'"+account+"'";
      //console.log(account);
      
      var tx =web3.eth.abi.encodeFunctionCall({name: 'burn', type: 'function', inputs: [{type: 'uint256', name: 'amount'}]}, [amount]);  

      return web3.eth.accounts.signTransaction({ from : account,
        to: TokenTunisie.address,data : tx, value: 0, gas: '0x1ffffffffffffe'},
        privateKey);
    }).then(function(transaction) {
      console.log("success signed Transaction");
      //callback("SUCCESS");
      return web3.eth.sendSignedTransaction(transaction.rawTransaction);
    }).then(function() {
      console.log("success Burn");
      callback("SUCCESS");
    }).catch(function(e) {
      console.log(e);
      callback("ERROR 404");
    });
  },
  createWallet: async function(callback) {
    //var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    var self = this;
    TokenTunisie.setProvider(web3.currentProvider);
    //console.log("app : ",account);
    var meta;
    
    var aa = await web3.eth.accounts.create();
    var en = await web3.eth.accounts.encrypt(aa.privateKey, 'test!');
    var a = await web3.eth.accounts.wallet.add(aa);
    var xx = await web3.eth.accounts.wallet.encrypt('test!');
    var ab = await web3.eth.accounts.wallet.toLocaleString('test!');

    callback(aa.address);
    
  }
}
  