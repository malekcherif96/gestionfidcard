pragma solidity >=0.4.22 <0.8.0;
 
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract TokenTunisie is Ownable, ERC20 {
    //decimals = 1;
    constructor() public ERC20("PointFid", "PointFid") {
        _setupDecimals(0);
    }

    function mint( uint256 amount) public{
        _mint(msg.sender, amount);
    }

    function burn(uint256 amount) public  {
        _burn(msg.sender, amount);
    }

    
    
    /*function transfer(address recipient, uint256 amount) public override returns (bool) {
        //_transfer(_msgSender(), recipient, amount);
        return true;
    }
*/
 }