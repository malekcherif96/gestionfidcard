module.exports = app => {
    const underShop = require("../controllers/underShop.controller.js");
    var router = require("express").Router();
    const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')
    // Create a new shopkeeper
    router.post("/add",underShop.create);

    // Retrieve all shopkeepers
    //router.get("/getNam/:code",shopkeeper.findbyname);
    router.get("/get/:id",underShop.findOne)
    router.delete('/delete/:id',underShop.deleteUnderShop)
    router.get('/getByShop/:id_Shopkeeper',underShop.findByshop)
    
    //router.get("/getnombre_cards/:id",shopkeeper.calcul_numbre_crads)
    router.get("/get",underShop.findAll);
    router.delete("/deleteall",underShop.deleteAll);
    app.use('/api/underShop', router);
     app.use(logErrorMiddleware)
    app.use(returnError)
}