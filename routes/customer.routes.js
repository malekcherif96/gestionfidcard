module.exports = app => {
const express = require('express')
const router = express.Router()
const { getCustomer, getCustomerById, saveCustomer, modifierCustomer, deleteCustomer, deleteAllCustomer} = require('../controllers/customerController.js')
const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')

router.get('/getall',getCustomer)
router.get('/get/:id',getCustomerById)
router.post('/add',saveCustomer)
//router.patch('/modify/:id',modifierCustomer)
router.delete('/delete/:id',deleteCustomer)
router.delete('/deleteAll/:id',deleteAllCustomer)
module.exports = router
app.use('/api/customer', router);

app.use(logErrorMiddleware)
  app.use(returnError)
}