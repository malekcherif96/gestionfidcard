module.exports = app => {
    const shopkeeper = require("../controllers/Shopkeeper.controller.js");
    var router = require("express").Router();
    const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')

    // Create a new shopkeeper
    router.post("/add",shopkeeper.create);

    // Retrieve all shopkeepers
    //router.get("/getNam/:code",shopkeeper.findbyname);
    router.get("/get/:id",shopkeeper.findOne)
    router.get("/get_card_numbre/:id",shopkeeper.getcarte_numbre)
    router.get("/getActivate",shopkeeper.findActivateShop);
    router.get("/getDeactivate",shopkeeper.findDeactivateShop);
    //router.get("/getnombre_cards/:id",shopkeeper.calcul_numbre_crads)
    router.get("/get",shopkeeper.findAll);
    router.delete("/deleteall",shopkeeper.deleteAll);
    
    app.use('/api/shopkeeper', router);

    app.use(logErrorMiddleware)
    app.use(returnError)
}

