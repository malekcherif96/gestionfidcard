
module.exports = app => {
   // const fidCardsPerso = require("../controllers/fidcardPerso.controller.js");
    const fidcardPerso = require("../controllers/fidCardPerso.controller.js")
    //const contract = require('truffle-contract');
    const path = require('path');
    const fileUpload = require('express-fileupload');
    const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')
    
    var router = require("express").Router();
    router.use(fileUpload({
        limits: { fileSize: 5 * 1024 * 1024 },
    }
    ));
    router.get("/customerId/:id",fidcardPerso.findAllByCustomerId);
    router.delete("/deleteAll", fidcardPerso.deleteAll);
    router.delete('/deleteCardID/:id', fidcardPerso.delete);
    router.get("/cardsid/:id",fidcardPerso.findOne);
    router.get("/cards",fidcardPerso.findAll);
    router.post("/add",fidcardPerso.create);

    app.use('/api/fidcardsPerso', router);
    app.use(logErrorMiddleware)
    app.use(returnError)
  }