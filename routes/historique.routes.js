module.exports = app => {
    const express = require('express')
    const router = express.Router()
    const historique = require('../controllers/Historique.controller.js')
    const path = require('path');
    const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')
    
    router.post('/add',historique.saveHistorique)


    router.get('/getall',historique.getHistorique)
    router.get('/get/:id',historique.getHistoriqueById)
    router.put('/modify/:id',historique.modifierHistorique)
    router.delete('/delete/:id',historique.deleteHistorique)
    router.get('/getByCustomer/:id',historique.getHistoriqueByCustomer)
    router.get('/getByShopkeeper/:id',historique.getHistoriqueByShopkeeper)
    router.get('/getByShopkeeperAndCustomer/:id/:input',historique.getHistoriqueByShopkeeperAndCustomerName)
    router.get('/getByShopkeeper/Only/:id',historique.getHistoriqueOnlyForShopkeeper)
    router.get('/getByUnderShop/:id',historique.getHistoriqueByUndershop)
    router.get('/getByFidcardId/:id',historique.getHistoriqueByFidcardId)
    router.get('/getTotalFid/:id',historique.getTotalFidByShopkeeper)
    router.get('/getTotalFidByDate/:id',historique.getTotalFidByDate)
    router.get('/getTotalFidMBByDate/:id',historique.getTotalFidMintBurnByDate)
    router.get('/getTotalFidByDateShop/:id',historique.getTotalFidByDateShop)
    router.get('/getTotalFidMBByDateShop/:id',historique.getTotalFidMintBurnByDateShop)
    router.get('/filter/:id',historique.filterHistoriques)
    router.get('/filter-undershop/:id',historique.filterHistoriquesByUndershop)
    router.get('/getTotalFid-under/:id',historique.getTotalFidByUndershop)
    router.get('/getTotalFid-shop/:id',historique.getTotalFidByShop)
    router.get('/filterTransaction/:id',historique.filterHistoriqueByShopkeeperAndUnderShopAndCustomerName)
    app.use('/api/historique', router);

    app.use(logErrorMiddleware)
    app.use(returnError)
}
