module.exports = app => {
  const fidCards = require("../controllers/fidcard.controller.js");
  //const contract = require('truffle-contract');
  const path = require('path');
  const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')

  var router = require("express").Router();

  // Create a new fidCard
  router.post("/add", fidCards.create);

  // Retrieve all todos
  router.get("/cards", fidCards.findAll);
  // Retrieve a single card by id
  router.get("/cardsid/:id", fidCards.findOne);

  router.get("/cardscode/:code", fidCards.findByCode);
  // Delete a Todo by id
  router.delete('/deleteCardID/:id', fidCards.delete);

  // update a Todo by id
  router.put('/updateCardID/:id', fidCards.update);



  // Create a new Tutorial
  router.delete("/deleteAll", fidCards.deleteAll);
  router.put("/mint/:id", fidCards.mint);
  router.put("/burn/:id", fidCards.burn);   
  //router.get("/geta/:id",fidCards.getBal);
  router.put("/burnProcessing", fidCards.burnFidProcessingOrder);
  router.put("/burnCanceled", fidCards.burnFidCanceledOrder);
  router.put("/burnComplited", fidCards.burnFidComplitedOrder);
  router.get("/cards/customerId/:id", fidCards.findAllByCustomerId);
  router.get("/cards/shopkeeperId/:id", fidCards.findFidCrdsByShopkeeper);
  router.get("/cards/shopkeeper/paginated",fidCards.findFidCardsByShopkeeperPaginated);
  router.get("/cards/admin/paginated",fidCards.findUserForAdminPaginated);
  router.get("/cards/findFidCardsByCustomerAndShopkeeper",fidCards.findFidCardsByCustomerAndShopkeeper);
  router.get("/cards/countAll",fidCards.countfid);
  
  app.use('/api/fidcards', router);
  
  app.use(logErrorMiddleware)
  app.use(returnError)
}