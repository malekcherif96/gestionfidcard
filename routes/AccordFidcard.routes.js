module.exports = app => {
const express = require('express')
const router = express.Router()
const { getAccordFidcard, getAccordFidcardById, createAccordFidcard, accordFidcardExistByShops,
  updateAccordFidcard, deleteAccordFidcard, getAccordFidcardByShops,getAccordFidcardByShopId, getShopsInAccordFidcardByShopId} = require('../controllers/accordFidcardController')
const { logErrorMiddleware, returnError } = require('../Handler/errorHandler')

router.get('/getall',getAccordFidcard);
router.get('/get/:id',getAccordFidcardById);
router.post('/add',createAccordFidcard);
router.get('/getByShops/:id', getAccordFidcardByShops);
router.patch('/update/:id',updateAccordFidcard);
router.delete('/delete/:id',deleteAccordFidcard);
router.get('/exist/:id',accordFidcardExistByShops);
router.get('/getByShopId/:id',getAccordFidcardByShopId);
router.get('/getShopsAccordByShopId/:id',getShopsInAccordFidcardByShopId);
//router.delete('/deleteAll/:id',deleteAllCustomer)
module.exports = router
app.use('/api/accordFidcard', router);

app.use(logErrorMiddleware)
  app.use(returnError)
}