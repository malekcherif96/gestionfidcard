"use strict";

module.exports = function (app) {
  var shopkeeper = require("../controllers/Shopkeeper.controller.js");

  var router = require("express").Router(); // Create a new shopkeeper


  router.post("/add", shopkeeper.create); // Retrieve all shopkeepers

  router.get("/get", shopkeeper.findAll);
  app.use('/api/shopkeeper', router);
};