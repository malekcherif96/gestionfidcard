"use strict";

module.exports = function (app) {
  var fidCards = require("../controllers/fidcard.controller.js");

  var router = require("express").Router(); // Create a new fidCard


  router.post("/add", fidCards.create); // Retrieve all todos

  router.get("/cards", fidCards.findAll); // Retrieve a single card by id

  router.get("/cardsid/:id", fidCards.findOne); // Delete a Todo by id

  router["delete"]('/deleteCardID/:id', fidCards["delete"]); // update a Todo by id

  router.put('/updateCardID/:id', fidCards.update); // Create a new Tutorial

  router["delete"]("/deleteAll", fidCards.deleteAll);
  router.get("/cards/customerId/:id", fidCards.findAllByCustomerId);
  app.use('/api/fidcards', router);
};