FROM node:10
WORKDIR /usr/src/app/
COPY package*.json ./
RUN npm install
ADD . /usr/src/app
EXPOSE 3002
VOLUME /usr/src/app/log
ADD wait-for-it.sh .
RUN ["chmod", "+x", "wait-for-it.sh"]
ENTRYPOINT ["./wait-for-it.sh", "MSEureka.aliases:8761", "--","node"]
CMD ["index.js"]
