const AccordFidcard = require("../models/accordFidcard");

const Api700Error = require('../Handler/api700Error');
async function getAccordFidcardByShops(shop1,shop2) {
    try {
        console.log("---------------------getAccordFidcardByShops ---------------------------")
        let filter = {$and:[
            { "shopkeepers": { $in: shop1 }}, 
            { "shopkeepers": { $in: shop2 }}
          ]};
          const accordFidcard = await AccordFidcard.find(filter).populate('shopkeeper');
          if (accordFidcard === null) {
            throw new Api700Error(`ACCORDFIDCARD_NOT_FOUND`);
          }

          return accordFidcard;
    } catch (error) {
        console.log(error);
    }
}

module.exports = {
    getAccordFidcardByShops
};