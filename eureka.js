const Eureka = require('eureka-js-client').Eureka;


const eureka = new Eureka({
    instance: {
        instanceId: 'MSFIDCARD',
        app: 'MSFIDCARD',
        hostName: process.env.HOSTNAME,
        ipAddr: process.env.ADRESSE,
        statusPageUrl: 'http://'+process.env.HOSTNAME+':'+process.env.PORT,
        port: {
            '$':process.env.PORT,
            '@enabled': 'true',
        },
        vipAddress: 'msfidcard',
        dataCenterInfo: {
            '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
            name: 'MyOwn',
        },
        registerWithEureka: true,
        fetchRegistry: true,
    },
    eureka: {
      host:process.env.EUREKA_HOST,
      port:process.env.EUREKA_PORT,
        servicePath: '/eureka/apps/',
        maxRetries: 10,
        requestRetryDelay: 2000,
    }
});
