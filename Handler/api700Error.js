const httpStatusCodes = require('./httpStatusCodes')
const BaseError = require('./baseError')

class Api700Error extends BaseError {
 constructor (
 name,
 statusCode = httpStatusCodes.ACCORDFIDCARD_NOT_FOUND,
 description = 'AccordFidcard does not existe .',
 isOperational = true
 ) {
 super(name, statusCode, isOperational, description)
 }
}

module.exports = Api700Error