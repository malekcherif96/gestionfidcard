
const httpStatusCodes = {
    OK: 200,
    BAD_REQUEST: 400,
    NOT_FOUND: 404,
    INTERNAL_SERVER: 500,
    INSUFFICIENT_BALANCE: 600,
    ACCORDFIDCARD_NOT_FOUND: 700
   }
   
module.exports = httpStatusCodes