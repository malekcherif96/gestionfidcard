function logError(err) {
    //console.log("-----> logError")
    console.error(err)
}

function logErrorMiddleware(err, req, res, next) {
    //console.log("-----> logErrorMiddleware")
    logError(err)
    next(err)
}

function returnError(err, req, res, next) {
    //console.log("-----> returnError", err.name)
    res.header("Content-Type", 'application/json')
    res.status(err.statusCode || 500).send(err)
}

function isOperationalError(error) {
    if (error instanceof BaseError) {
        return error.isOperational
    }
    return false
}

module.exports = {
    logError,
    logErrorMiddleware,
    returnError,
    isOperationalError
}