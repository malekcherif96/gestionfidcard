const httpStatusCodes = require('./httpStatusCodes')
const BaseError = require('./baseError')

class Api600Error extends BaseError {
 constructor (
 name,
 statusCode = httpStatusCodes.INSUFFICIENT_BALANCE,
 description = 'Balance insuffisant.',
 isOperational = true
 ) {
 super(name, statusCode, isOperational, description)
 }
}

module.exports = Api600Error