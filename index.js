/* ****************prod***************************/
require('dotenv').config();
var __importDefault = (this && this.__importDefault) || function (mod) {
  return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const serve_static_1 = __importDefault(require("serve-static"));
const mongoose_1 = __importDefault(require("mongoose"));
//const cors_1 = __importDefault(require("cors"));
const Web3 = require('web3')
const shopkeeperController = require("./controllers/Shopkeeper.controller.js");
const underShopController =require("./controllers/underShop.controller.js");
const customerController= require('./controllers/customerController.js')
const fidcardController= require('./controllers/fidcard.controller')

/*  MONGO_HOSTNAME="213.32.95.79"
MONGO_HOSTNAME="195.154.51.68"
MONGO_PORT="27017"
MONGO_DB="195.154.51.68"
MESSAGE_QUEUE="amqp://guest:guest@195.154.51.68:5672"
HOSTNAME="127.0.0.1"
ADRESSE="127.0.0.1:3006"
PORT="3006"
EUREKA_HOST="195.154.51.68"
EUREKA_PORT="8761"
DATABASE="195.154.51.68"
DBPORT="27022" 
*/

/* const {
  MONGO_HOSTNAME,
  MONGO_PORT,
  MONGO_DB,
  MESSAGE_QUEUE,
  HOSTNAME,
  ADRESSE,
  PORT,
  EUREKA_HOST,
  EUREKA_PORT,
  DATABASE,
  DBPORT  } =  process.env ;  */

const Eureka = require('eureka-js-client').Eureka;


  const eureka = new Eureka({
    instance: {
        instanceId: 'MSFIDCARD',
        app: 'MSFIDCARD',
        hostName: process.env.HOSTNAME,
        ipAddr: process.env.ADRESSE,
        statusPageUrl: 'http://'+process.env.HOSTNAME+':'+process.env.PORT,
        port: {
            '$':process.env.PORT,
            '@enabled': 'true',
        },
        vipAddress: 'msfidcard',
        dataCenterInfo: {
            '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
            name: 'MyOwn',
        },
        registerWithEureka: true,
        fetchRegistry: true,
    },
    eureka: {
      host:process.env.EUREKA_HOST,
      port:process.env.EUREKA_PORT,
        servicePath: '/eureka/apps/',
        maxRetries: 10,
        requestRetryDelay: 2000,
    }
});
eureka.logger.level('debug');
eureka.start(function (error) {
    console.log(error || 'complete');
});




var amqp = require('amqplib/callback_api');
amqp.connect(process.env.MESSAGE_QUEUE, function(error0, connection) {
if (error0) {
  throw error0;
}
connection.createChannel(function(error1, channel) {
  
  if (error1) {
    throw error1;
  }
  var exchange = 'new_shopkeeper_exchange';

  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('new_shopkeeper1', {
    exclusive: false,
    durable: true,
    autoDelete: false
  }, function(error2, q) {
    if (error2) {
      throw error2;
    }
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
    channel.bindQueue(q.queue, exchange, '');
    channel.consume(q.queue, function(msg) {
      if(msg.content) {
        const user = JSON.parse(msg.content.toString());
        const shopkeeper = new Shopkeeper({
          brandName: user.brandName,
          logo: user.logo,
          _id: user.id
        });
        shopkeeperController.addRabbitMQ(shopkeeper);
          //var obj = ;
          console.log(" [x] %s", user);
          console.log(" [x] %s", shopkeeper);
        }
    }, {
      noAck: true
    });
  });
});
/*------------New under-shop---------*/
connection.createChannel(function(error1, channel) {
 
  if (error1) {
    throw error1;
  }
  var exchange = 'new_under_exchange';

  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('new_under_shop_fid', {
    exclusive: false,
    durable: true,
    autoDelete: false
  }, function(error2, q) {
    if (error2) {
      throw error2;
    }
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
    channel.bindQueue(q.queue, exchange, '');
    
 

    channel.consume(q.queue, function(msg) {
      if(msg.content) {
        const user = JSON.parse(msg.content.toString());
        console.log('===============',user)
        const underShop = new UnderShop({
          id_Shopkeeper: user. id_Shopkeeper,
          firstName: user.firstName,
          lastName:user.lastName,
          role:user.role,
          id_Store:user.id_Store,
          storeName:user.storeName,
          phoneNumber:user.phoneNumber,
          _id: user.id
          
        });
        underShopController.addRabbitMQ(underShop);
          //var obj = ;
          console.log(" [x] %s", user);
          console.log(" [x] %s", underShop);
        }
    }, {
      noAck: true
    });
  });
});
/* ---------------UPDATE customer---------------- */
connection.createChannel(function(error1, channel) {
  if (error1) {
    throw error1;
  }
  var exchange = 'update_customer_exchange';
  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('update_customer_fid', {
    exclusive: false,
    durable: true,
    autoDelete: false
    }, function(error2, q) {
      if (error2) {
        throw error2;
      }
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');

      channel.consume(q.queue, function(msg) {
        console.log("msg.content.toString()", msg.content.toString());
        if(msg.content) {
          const user = JSON.parse(msg.content.toString());
          const customer = new Customer({
            firstName: user.firstName,
            lastName: user.lastName,
            phoneNumber: user.phoneNumber,
            imageName:user.imageName,
            _id: user.id
          });
         
          customerController.updateRabbitMQ(customer);
            console.log(" [x] %s", customer);
          }
      }, {
        noAck: true
      });
    });
});
/* ---------------UPDATE SHOPKEEPER---------------- */
connection.createChannel(function(error1, channel) {
  if (error1) {
    throw error1;
  }
  var exchange = 'update_shopkeeper_exchange';
  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('update_shopkeeper_fidCard', {
    exclusive: false,
    durable: true,
    autoDelete: false
    }, function(error2, q) {
      if (error2) {
        throw error2;
      }
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');

      channel.consume(q.queue, function(msg) {
        console.log("msg.content.toString()", msg.content.toString());
        if(msg.content) {
          const user = JSON.parse(msg.content.toString());
          const shopkeeper = new Shopkeeper({
            brandName: user.brandName,
            logo: user.logo,
            _id: user.id,
            is_Activate:user.is_Activate,
            tauxRemise:user.tauxRemise,
            tauxConversion:user.tauxConversion,
            taxIdentificationNumber:user.taxIdentificationNumber
          });
          shopkeeperController.updateRabbitMQ(shopkeeper);
            console.log(" [x] %s", shopkeeper);
          }
      }, {
        noAck: true
      });
    });
});
/*------------New Customer---------*/
connection.createChannel(function(error1, channel) {

  if (error1) {
    throw error1;
  }
  var exchange = 'new_customer_exchange';

  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('new_customer1', {
    exclusive: false,
    durable: true,
    autoDelete: false
  }, function(error2, q) {
    if (error2) {
      throw error2;
    }
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
    channel.bindQueue(q.queue, exchange, '');

    channel.consume(q.queue, function(msg) {
      if(msg.content) {
        const user = JSON.parse(msg.content.toString());
        const customer = new Customer({
          firstName: user.firstName,
          lastName: user.lastName,
          phoneNumber: user.phoneNumber,
          gender:user.gender,
          _id: user.id
        });
        customerController.addCustomerRabbitMQ(customer);
          //var obj = ;
          console.log(" [x] %s", user);
          console.log(" [x] %s", customer);
        }
    }, {
      noAck: true
    });
  });
});
/*--------------------- New Order ----------------*/
connection.createChannel(function(error1, channel) {
  if (error1) {
    throw error1;
  }
  var exchange = 'notif_commerce_exchange';
  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('new_commande_complited', {
    exclusive: false,
    durable: true,
    autoDelete: false
    }, function(error2, q) {
      if (error2) {
        throw error2;
      }
      console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
      channel.bindQueue(q.queue, exchange, '');

      channel.consume(q.queue, function(msg) {
        console.log("new_commande_complited :", msg.content.toString());
        if(msg.content) {
          const user = JSON.parse(msg.content.toString());
          
          fidcardController.sendPointFidNewOrder(user);
            //console.log(" [x] %s", user);
          }
      }, {
        noAck: true
      });
    });
});
/*-----------------------------Delete Shopkeeper---------------------------------*/
connection.createChannel(function(error1, channel) {
  
  if (error1) {
    throw error1;
  }
  var exchange = 'delete_shopkeeper_exchange';

  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('delete_shop', {
    exclusive: false,
    durable: true,
    autoDelete: false
  }, function(error2, q) {
    if (error2) {
      throw error2;
    }
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
    channel.bindQueue(q.queue, exchange, '');

    channel.consume(q.queue, function(msg) {
      if(msg.content) {
        const idShop = JSON.parse(msg.content.toString());

        shopkeeperController.deleteRabbitMQ(idShop);
          console.log(" [x] %s", idShop);
          
        }
    }, {
      noAck: true
    });
  });
});


/*-----------------------------Delete Customer---------------------------------*/
connection.createChannel(function(error1, channel) {
  
  if (error1) {
    throw error1;
  }
  var exchange = 'delete_customer_exchange';

  channel.assertExchange(exchange, 'fanout', {
    durable: true,
    autoDelete: false
  });

  channel.assertQueue('delete_customer_MSFid', {
    exclusive: false,
    durable: true,
    autoDelete: false
  }, function(error2, q) {
    if (error2) {
      throw error2;
    }
    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
    channel.bindQueue(q.queue, exchange, '');

    channel.consume(q.queue, function(msg) {
      if(msg.content) {
        const idCustomer = JSON.parse(msg.content.toString());

        customerController.deleteRabbitMQ(idCustomer);

          console.log(" [x] %s", idCustomer);
          
        }
    }, {
      noAck: true
    });
  });
});


});


var Database;


/* Instancier Express */
const app = express_1.default();
/* Middleware bodyParser pour parser le corps des requêtes en Json*/
app.use(body_parser_1.default.json({limit: '50mb'}));
/* Middlware pour configurer le dossier des ressources statique*/
app.use(serve_static_1.default("public"));
/* Actvier CORS*/
//app.use(cors_1.default());
app.use(body_parser_1.default.urlencoded({extended:true}));


const MongoClient = require('mongodb').MongoClient;
const Shopkeeper = require("./models/Shopkeeper.js");
const UnderShop = require("./models/underShop.js");
const Customer = require("./models/customer.model.js");
/*const uri = "mongodb+srv://houssemmh:26935040@cluster0.woupd.mongodb.net/mscards?retryWrites=true&w=majority";
const client = new MongoClient(uri, { useNewUrlParser: true });
mongoose_1.default.connect(uri,(err)=>{
      if(err){ console.log(err); }
      else{ console.log("Mongo db connection sucess");}
});*/
let uri="mongodb://"+process.env.DATABASE+":"+process.env.DBPORT+"/ManageFidCard";
  const client = new MongoClient(uri, { useNewUrlParser: true });
  mongoose_1.default.connect(uri,(err)=>{
          if(err){ console.log(err); }
        else{ console.log("Mongo db connection sucess"); }
  });


require("./routes/fidCardPerso.routes")(app);
require("./routes/fidCard.routes")(app);
require("./routes/shopkeeper.routes")(app);
require("./routes/historique.routes")(app);
require("./routes/customer.routes")(app);
require("./routes/underShop.routes")(app);
require("./routes/underShop.routes")(app);
require("./routes/AccordFidcard.routes")(app);
/* Démarrer le serveur*/
app.listen(process.env.PORT, () => {
  console.log("Server Started on port %d", process.env.PORT);
});