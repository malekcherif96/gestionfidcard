const AccordFidcard = require('../models/accordFidcard')

const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')

exports.getAccordFidcard = async (req, res, next) => {
    console.log("--------------- AccordFidcard: getAccordFidcard STARTED -------------");
    try {
      const accordFidcard = await AccordFidcard.find().populate("shopkeepers");
      //historiques.catch()
      if (accordFidcard === null) {
        throw new Api404Error(`AccordFidcard not found`)
      }
      res.status(200).send(accordFidcard);
    } catch (err) {
      next(err)
    }
  }
  
  exports.getAccordFidcardById = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: getAccordFidcardById STARTED -------------");
      const accordFidcard = await AccordFidcard.findById(req.params.id).populate('shopkeepers');
      if (accordFidcard === null) {
        throw new Api404Error(`AccordFidcard with id ${req.params.id} not found`)
      }
      res.status(200).send(accordFidcard)
    } catch (err) {
      next(err)
    }
  }

exports.createAccordFidcard = async (req, res, next) => {

    try {
      console.log("--------------- AccordFidcard: createAccordFidcard STARTED -------------");
      const accordFidcard = new AccordFidcard({
        type: req.body.type,
        dateCreation: req.body.dateCreation,
        dateExpiration: req.body.dateExpiration,
        ceiling: req.body.ceiling,
        shopkeepers: req.body.shopkeepers,
        contractFile: req.body.contractFile
      })
  
      let acc = await accordFidcard.save();

      if (acc) {
        res.status(200).send(accordFidcard);
      }
      
      /*.then(accordFidcard => {
        res.status(200).send(accordFidcard);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          throw new Api404Error(`AccordFidcard with ObjectId ${req.params.id} not created.`)
        }
        throw new Api500Error(`AccordFidcard with id ${req.params.id} not found.`)
      });*/

      process.on('unhandledRejection', error => {
        throw error
        // process.exit(1);
      })
      process.on('uncaughtException', error => {
        throw error
      })
    } catch (err) {
      next(err)
    }
  
  }

  exports.updateAccordFidcard = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: updateAccordFidcard STARTED -------------");
      const accordFidcard = await accordFidcard.findByIdAndUpdate(req.params.id,
        {
            type: req.body.type,
            dateCreation: req.body.dateCreation,
            dateExpiration: req.body.dateExpiration,
            ceiling: req.body.ceiling,
            shopkeepers: req.body.shopkeepers,
            contractFile: req.body.contractFile
        });
      
      if (accordFidcard === null) {
        throw new Api500Error(`AccordFidcard with id ${req.params.id} not updated`)
      }
      res.status(200).send(accordFidcard);
    } catch (err) {
      next(err)
    }
  }
  
  exports.deleteAccordFidcard = (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: deleteAccordFidcard STARTED -------------");
      AccordFidcard.deleteOne({ _id: req.params.id }).then(
        () => {
          res.status(200).send({
            message: 'Deleted!'
          });
        }
      ).catch(
        (error) => {
          throw new Api500Error(error)
        }
      );
    } catch (err) {
      next(err)
    }
  };

  exports.getAccordFidcardByShops = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: getAccordFidcardByShops STARTED -------------");
      let filter = {$and:[
        { "shopkeepers": { $in: req.params.id }}, 
        { "shopkeepers": { $in: req.query.idShop2 }}
      ]};
      const accordFidcard = await AccordFidcard.find(filter).populate('shopkeepers');
      if (accordFidcard === null) {
        throw new Api404Error(`AccordFidcard with id ${req.params.id} not found`)
      }
      res.status(200).send(accordFidcard)
    } catch (err) {
      next(err)
    }
  }

  exports.accordFidcardExistByShops = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: getAccordFidcardByShops STARTED -------------");
      let filter = { "shopkeepers": { $in: req.params.id }};
      const accordFidcard = await AccordFidcard.find(filter);
      if (accordFidcard === null) {
        return false
        //throw new Api404Error(`AccordFidcard with id ${req.params.id} not found`)
      }
      res.status(200).send(true)
    } catch (err) {
      next(err)
    }
  }

  exports.getAccordFidcardByShopId = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: getAccordFidcardByShops STARTED -------------");
      let filter = { "shopkeepers": { $in: req.params.id }};
      const accordFidcard = await AccordFidcard.find(filter).populate('shopkeepers');
      if (accordFidcard === null) {
        //return false
        throw new Api500Error(`AccordFidcard with id shop ${req.params.id} not found`)
      }
      res.status(200).send(accordFidcard)
    } catch (err) {
      next(err)
    }
  }

  exports.getShopsInAccordFidcardByShopId = async (req, res, next) => {
    try {
      console.log("--------------- AccordFidcard: getAccordFidcardByShops STARTED -------------");
      let date = new Date();
      let filter = {$and:[
        { "shopkeepers": { $in: req.params.id }}, 
        { "dateExpiration": { $gte: date }}
      ]};
      const accordFidcard = await AccordFidcard.find(filter)//.populate('shopkeepers');
     
      if (accordFidcard === null) {
        //return false
        throw new Api500Error(`AccordFidcard with id shop ${req.params.id} not found`)
      }

      let shops=[];
      accordFidcard.forEach(accord =>{
        accord.shopkeepers.forEach(element => {
          if(element !== req.params.id)
             shops.push(element)
        });
        
      })
      console.log(shops)
      
      res.status(200).send(shops)
    } catch (err) {
      next(err)
    }
  }