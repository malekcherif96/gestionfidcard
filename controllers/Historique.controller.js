const FidelityCard = require("../models/fidCard.model.js");
const Historique = require("../models/historique.model.js");
const Customer = require("../models/customer.model.js");
const Shopkeeper = require("../models/Shopkeeper.js");
const HistoriqueController = require('../controllers/Historique.controller.js')
const date = require('date-and-time')
const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')
exports.getHistorique = async (req, res, next) => {
  console.log("--------------- Historique: getHistorique STARTED -------------");
  try {
    const historiques = await Historique.find().populate("fidcard");
    //historiques.catch()
    if (historiques === null) {
      throw new Api404Error(`Historique not found`)
    }
    res.status(200).send(historiques);
  } catch (err) {
    next(err)
  }
}

exports.getHistoriqueById = async (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueById STARTED -------------");
    const historique = await Historique.findById(req.params.id).populate('fidcard');
    if (historique === null) {
      throw new Api404Error(`Historique with id ${req.params.id} not found`)
    }
    res.status(200).send(historique)
  } catch (err) {
    next(err)
  }
}

exports.saveHistorique = async (req, res, next) => {

  try {
    console.log("--------------- Historique: saveHistorique STARTED -------------");
    const historique = new Historique({
      type: req.body.type,
      dateCreation: req.body.dateCreation,
      pointFid: req.body.pointFid,
      shopkeeper: req.body.shopkeeper.id,
      undershop: req.body.undershop,
      customer: req.body.customer.id,
      fidcard: req.body.fidcard._id,
      shopkeeperReceiver: req.body.shopkeeperReceiver,
      customerReceiver: req.body.customerReceiver,
      id_Store: req.body.id_Store
      /*coupons:JSON.parse(req.body.coupons)*/
    })

    historique.save().then(historique => {
      res.status(200).send(historique);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        throw new Api404Error(`FidelityCard with ObjectId ${req.params.id} not found.`)
      }
      throw new Api500Error(`FidelityCard with id ${req.params.id} not found.`)
    });

    /* Shopkeeper.findById(req.body.shopkeeper.id).then(data => {
      historique.shopkeeper = data;
      console.log("historique.shopkeeper =", historique.shopkeeper)
      return Customer.findById(req.body.customer.id)
    }).then(data => {
      historique.customer = data;
      console.log("historique.customer =", historique.customer)
      return FidelityCard.findById(req.body.fidcard._id)
    }).then(data => {
      historique.fidcard = data;
      console.log("historique.fidcard =", historique.fidcard)
      return historique.save()
    }).then(historique => {
      res.status(200).send(historique);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        throw new Api404Error(`FidelityCard with ObjectId ${req.params.id} not found.`)
      }
      throw new Api500Error(`FidelityCard with id ${req.params.id} not found.`)
    }); */

    process.on('unhandledRejection', error => {
      throw error
      // process.exit(1);
    })
    process.on('uncaughtException', error => {
      throw error
    })
  } catch (err) {
    next(err)
  }

}

exports.modifierHistorique = async (req, res, next) => {
  try {
    
    console.log("--------------- Historique: modifierHistorique STARTED -------------");
    const historique = await Historique.findByIdAndUpdate(req.params.id,
      {
        type: req.body.type,
        dateCreation: req.body.dateCreation,
        pointFid: req.body.pointFid,
        shopkeeper: req.body.shopkeeper.id,
        undershop: req.body.undershop,
        customer: req.body.customer.id,
        fidcard: req.body.fidcard._id,
        shopkeeperReceiver: req.body.shopkeeperReceiver,
        customerReceiver: req.body.customerReceiver,
        id_Store: req.body.id_Store
        /*coupons:JSON.parse(req.body.coupons)*/
      });
    
    if (historique === null) {
      throw new Api500Error(`Historique with id ${req.params.id} not updated`)
    }
    res.status(200).send(historique);
  } catch (err) {
    next(err)
  }
}

exports.deleteHistorique = (req, res, next) => {
  try {
    console.log("--------------- Historique: deleteHistorique STARTED -------------");
    Historique.deleteOne({ _id: req.params.id }).then(
      () => {
        res.status(200).send({
          message: 'Deleted!'
        });
      }
    ).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );
  } catch (err) {
    next(err)
  }
};

exports.getHistoriqueByShopkeeper = (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueByShopkeeper STARTED -------------");
    const { page = 1, limit = 20 } = req.query;
    //console.log(req.params.id);
    var mysort = { dateCreation: -1, };
    let filter = { "type": { $ne: "Transfer" } }
    Historique.find(filter).where('shopkeeper').equals(req.params.id).sort(mysort)
      .populate("undershop").populate("customer").limit(limit * 1).skip((page - 1) * limit).then(historique => {


        res.status(200).send(historique);
      }).catch(
        (error) => {
          throw new Api500Error(error)
        }
      );

  } catch (err) {
    next(err)
  }
};
exports.getHistoriqueByShopkeeperAndCustomerName = async (req, res, next) => {
  historiqueWithCustomer = []
  try {

    var query = req.params.input.toLowerCase();
    console.log("--------------- Historique: getHistoriqueByShopkeeperAndCustomerName STARTED -------------");
    const { page = 1, limit = 20 } = req.query;
    //console.log(req.params.id);
    var mysort = { dateCreation: -1, };
    //let filter ={ "type": {$ne:"Transfer"}}
    const historiques = await Historique.find().where('shopkeeper').equals(req.params.id).sort(mysort)
      .populate("undershop").populate(
        {
          path: 'customer',

          match: {

            $or: [
              { "firstName": { $regex: new RegExp('.*' + query + '.*', "i") } },
              { "lastName": { $regex: new RegExp('.*' + query + '.*', "i") } },
            ]

          }

        }).limit(limit * 1).skip((page - 1) * limit)
    for (let index = 0; index < historiques.length; index++) {
      if (historiques[index].customer != null) {
        historiqueWithCustomer.push(historiques[index])
      }

    }
    res.status(200).send(historiqueWithCustomer);


  } catch (err) {
    throw new Api500Error(error)

  }
};
exports.getHistoriqueByUndershop = (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueByUndershop STARTED -------------");
    const { page = 1, limit = 20 } = req.query;
    console.log(req.params.id);
    var mysort = { dateCreation: -1 };
    let filter = { "type": { $ne: "Transfer" } };
    Historique.find(filter).sort(mysort).limit(limit * 1).skip((page - 1) * limit).where('undershop').equals(req.params.id).populate("customer").populate("undershop").then(historique => {
      console.log(historique)
      res.status(200).send(historique);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );

  } catch (err) {
    next(err)
  }
};
exports.getHistoriqueOnlyForShopkeeper = (req, res, next) => {
  console.log("--------------- Historique: getHistoriqueOnlyForShopkeeper STARTED -------------");
  var filter = {
    $or: [
      { "undershop": { $eq: '' } }
    ]
  }
  try {
    const { page = 1, limit = 20 } = req.query;
    console.log(req.params.id);
    var mysort = { dateCreation: -1 };
    let filter = { "type": { $ne: "Transfer" } };
    Historique.find({ $or: [{ "undershop": { $eq: '' } }, { "undershop": { $eq: req.params.id } }] }).sort(mysort).limit(limit * 1).skip((page - 1) * limit)
      .populate("customer").then(historique => {
        console.log(historique)
        res.status(200).send(historique);
      }).catch(
        (error) => {
          throw new Api500Error(error)
        }
      );

  } catch (err) {
    next(err)
  }
};

exports.getTotalFidByShopkeeper = (req, res, next) => {
  try {
    console.log("--------------- Historique: getTotalFidByShopkeeper STARTED -------------");

    Historique.aggregate([
      {
        $match: { $and: [{ 'shopkeeper': req.params.id }, { "type": { $ne: "Transfer" } }] }
      },
      {
        $group: {
          _id: req.params.id,
          total: { $sum: "$pointFid" }
        }
      }]

    ).then(historique => {
      res.status(200).send(historique);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );
  } catch (err) {
    next(err)
  }
};

exports.getTotalFidByUndershop = (req, res, next) => {
  try {

    console.log("--------------- Historique: getTotalFidByUndershop STARTED -------------");
    Historique.aggregate([
      {
        $match: { $and: [{ 'undershop': req.params.id }, { "type": { $ne: "Transfer" } }] }
      },
      {
        $group: {
          _id: req.params.id,
          total: { $sum: "$pointFid" }
        }
      }]

    ).then(historique => {
      res.status(200).send(historique);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );
  } catch (err) {
    next(err)
  }
};


exports.getTotalFidByShop = (req, res, next) => {
  try {

    console.log("--------------- Historique: getTotalFidByShop STARTED -------------");
    Historique.aggregate([
      {
        $match: { $and: [{ 'id_Store': req.params.id }, { "type": { $ne: "Transfer" } }] }
      },
      {
        $group: {
          _id: req.params.id,
          total: { $sum: "$pointFid" }
        }
      }]

    ).then(historique => {
      res.status(200).send(historique);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );
  } catch (err) {
    next(err)
  }
};

exports.getHistoriqueByCustomer = (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueByCustomer STARTED -------------");
    const { page = 1, limit = 10 } = req.query;
    var mysort = { dateCreation: -1 };
    console.log(req.params.id);
    Historique.find().sort(mysort).limit(limit * 1).skip((page - 1) * limit).where('customer').equals(req.params.id)
      .then(historique => {
        console.log(historique)
        res.status(200).send(historique);
      }).catch(
        (error) => {
          throw new Api500Error(error)
        }
      );

  } catch (err) {
    next(err)
  }
};

exports.getHistoriqueByFidcardId = (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueByFidcardId STARTED -------------");
    const { page = 1, limit = 5 } = req.query;
    console.log(req.params.id);
    var mysort = { dateCreation: -1 };
   /*  Historique.deleteMany({"pointFid":{$eq:null}}).then(hi => {
      console.log(hi);
    }) */
    Historique.find().sort(mysort).limit(limit * 1).skip((page - 1) * limit).where('fidcard').equals(req.params.id).then(historique => {
      console.log(historique)
      res.status(200).send(historique);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );

  } catch (err) {
    next(err)
  }
};


exports.getHistoriqueByType = async (req, res, next) => {
  try {
    console.log("--------------- Historique: getHistoriqueByType STARTED -------------");
    filter = {
      $and: [{ "shopkeeper": { $eq: req.params.id } },
      { "type": { $eq: req.params.type } }]
    }
    var mysort = { dateCreation: -1 };
    const { page = 1, limit = 5 } = req.query;
    const historiques = await Historique.find(filter).sort(mysort).limit(limit * 1).skip((page - 1) * limit).populate("undershop").populate("customer");
    if (historiques === null) {
      throw new Api500Error(`Historique By Type  not found`)
    }
    res.status(200).send(historiques)
  } catch (err) {
    next(err)
  }
};

exports.filterHistoriqueByShopkeeperAndUnderShopAndCustomerName = async (req, res, next) => {
  console.info("----- API findFidCrdsByShopkeeperAndCustomerName STARTED -----");
  var fidelitys = []
  console.log("id shop", req.params.id)
  console.log("id Undershop", req.query.idUnder)
  const page = parseInt(req.query.page)
  const limit = parseInt(req.query.limit)
  if (req.query.idUnder != '' || req.query.search != '' || req.query.day != '' || req.query.type != '' || req.query.min_fid != '' || req.query.max_fid != '') {

    if (req.query.idUnder != '' && req.query.search == '' && req.query.day == '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "type": { $ne: "Transfer" } },
              { "shopkeeper": { $eq: req.params.id } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day == '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day != '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: date } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day == '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $eq: req.query.type } },
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day == '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day != '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: date } },
              { "type": { $ne: "Transfer" } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day != '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: date } },
              { "type": { $eq: req.query.type } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day == '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $eq: req.query.type } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day == '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $eq: req.query.type } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day == '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "type": { $ne: "Transfer" } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day != '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $eq: req.query.type } },

              { "dateCreation": { $gte: date } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day != '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "type": { $ne: "Transfer" } },
              { "dateCreation": { $gte: date } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search == '' && req.query.day == '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },

              { "type": { $eq: req.query.type } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder != '' && req.query.search != '' && req.query.day != '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "type": { $eq: req.query.type } },
              { "undershop": { $eq: req.query.idUnder } },
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: date } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day == '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      console.log('iha hne ')
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day != '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day == '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "type": { $eq: req.query.type } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day == '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day != '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "type": { $eq: req.query.type } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day != '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "type": { $eq: req.query.type } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search != '' && req.query.day == '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [

              { "type": { $eq: req.query.type } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              },
              { "shopkeeper": { $eq: req.params.id } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day != '' && req.query.type == '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day != '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $eq: req.query.type } },
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day != '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "type": { $ne: "Transfer" } }
            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day != '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "dateCreation": { $gte: date } },
              { "type": { $eq: req.query.type } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day == '' && req.query.type != '' && req.query.min_fid == '' && req.query.max_fid == '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "type": { $eq: req.query.type } },
              { "shopkeeper": { $eq: req.params.id } },


            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day == '' && req.query.type != '' && req.query.min_fid != '' && req.query.max_fid != '') {

      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "type": { $eq: req.query.type } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },


            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    } else if (req.query.idUnder == '' && req.query.search == '' && req.query.day == '' && req.query.type == '' && req.query.min_fid != '' && req.query.max_fid != '') {
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [

              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "type": { $ne: "Transfer" } }

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    }
    else {
      var date = new Date((req.query.day).replace(" ", "+"));
      date.setHours(0, 0, 0, 0);
      var aggr = [
        {
          "$lookup": {
            "from": "customers",
            "localField": "customer",
            "foreignField": "_id",
            "as": "listeCustomer"
          }
        },
        { "$unwind": "$listeCustomer" },
        {
          "$lookup": {
            "from": "undershops",
            "localField": "undershop",
            "foreignField": "_id",
            "as": "listeUndershop"
          }
        },
        {
          "$unwind": {
            path: "$listeUndershop",
            preserveNullAndEmptyArrays: true
          }
        },

        {
          "$match": {
            $and: [
              { "type": { $eq: req.query.type } },
              { "dateCreation": { $gte: date } },
              { "shopkeeper": { $eq: req.params.id } },
              { "pointFid": { $gte: Number(req.query.min_fid), $lte: Number(req.query.max_fid) } },
              { "undershop": { $eq: req.query.idUnder } },

              {
                $or: [
                  { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },

                  { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                  { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.search + '.*', "i") } },
                ]
              }

            ]
          },

        },
        {
          $project: {
            _id: 1,
            type: 1,
            dateCreation: 1,
            pointFid: 1,
            fidcard: 1,
            shopkeeper: 1,
            undershop: 1,
            id_Store: 1,
            underShopFirstName: "$listeUndershop.firstName",
            underShopLastName: "$listeUndershop.lastName",
            underShopPhoneNumber: "$listeUndershop.phoneNumber",
            underShopStoreName: "$listeUndershop.storeName",
            firstName: "$listeCustomer.firstName",
            lastName: "$listeCustomer.lastName",
            phoneNumber: "$listeCustomer.phoneNumber"
          }
        },
        { $sort: { dateCreation: -1 } },
        {
          '$facet': {
            metadata: [{ $count: "total" }, { $addFields: { page: page } }],
            data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
          }
        }
      ]
    }

  } else {
    var aggr = [
      {
        "$lookup": {
          "from": "customers",
          "localField": "customer",
          "foreignField": "_id",
          "as": "listeCustomer"
        }
      },
      { "$unwind": "$listeCustomer" },
      {
        "$lookup": {
          "from": "undershops",
          "localField": "undershop",
          "foreignField": "_id",
          "as": "listeUndershop"
        }
      },
      {
        "$unwind": {
          path: "$listeUndershop",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        "$match": { $and: [
                  { "shopkeeper": req.params.id }, 
                  { "type": { $ne: "Transfer" } }] },

      },
      {
        $project: {
          _id: 1,
          type: 1,
          dateCreation: 1,
          pointFid: 1,
          fidcard: 1,
          shopkeeper: 1,
          undershop: 1,
          id_Store: 1,
          underShopFirstName: "$listeUndershop.firstName",
          underShopLastName: "$listeUndershop.lastName",
          underShopPhoneNumber: "$listeUndershop.phoneNumber",
          underShopStoreName: "$listeUndershop.storeName",
          firstName: "$listeCustomer.firstName",
          lastName: "$listeCustomer.lastName",
          phoneNumber: "$listeCustomer.phoneNumber"
        }
      },
      { $sort: { dateCreation: -1 } },
      {
        '$facet': {
          metadata: [{ $count: "total" }, { $addFields: { page: page } }],
          data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
        }
      }
    ]
  }
  try {
    console.log(aggr)
    Historique.aggregate(aggr).then(data => {

      res.status(200).send(data[0]);
    })
  } catch (err) {
    next(err)
  }
}

exports.filterHistoriques = async (req, res, next) => {
  try {

    console.log("--------------- Historique: filterHistoriques STARTED -------------");


    var mysort = { dateCreation: -1 };
    const { page = 1, limit = 5 } = req.query;

    if (req.query.type === "all") {
      if (req.query.day != "" && (req.query.lower != req.query.upper)) {
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "shopkeeper": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "pointFid": { $gte: Number(req.query.lower), $lte: Number(req.query.upper) } }]
        };


      } else if (req.query.day === "" && (req.query.lower != req.query.upper)) {
        console.log("filter by point fid");
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "shopkeeper": { $eq: req.params.id } },
            { "pointFid": { $gte: Number(req.query.lower), $lte: Number(req.query.upper) } }]
        };
      } else {

        console.log("filter by date ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "shopkeeper": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } }
          ]
        };
      }

    } else {
      if (req.query.day != "" && (req.query.lower != req.query.upper)) {
        console.log("filter by type && by date && pointfid ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "shopkeeper": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "pointFid": { $gte: req.query.lower, $lte: req.query.upper } },
            { "type": { $eq: req.query.type } }
          ]
        };

      } else if (req.query.day === "" && (req.query.lower != req.query.upper)) {
        console.log("filter by type && by point fid");
        filter = {
          $and: [
            { "shopkeeper": { $eq: req.params.id } },
            { "pointFid": { $gte: req.query.lower, $lte: req.query.upper } },
            { "type": { $eq: req.query.type } }
          ]
        };
      } else if (req.query.day != "") {

        console.log("filter by type && by date ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "shopkeeper": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "type": { $eq: req.query.type } }
          ]
        };
      } else {
        console.log("filter by type ");
        filter = {
          $and: [
            { "shopkeeper": { $eq: req.params.id } },
            { "type": { $eq: req.query.type } }
          ]
        };
      }

    }

    const historiques = await Historique.find(filter).sort(mysort).limit(limit * 1).skip((page - 1) * limit).populate("undershop").populate("customer");

    if (historiques === null) {
      throw new Api500Error(`Filter Historique not found`)
    }
    res.status(200).send(historiques);
  } catch (err) {
    next(err)
  }
};







exports.filterHistoriquesByUndershop = async (req, res, next) => {
  try {
    console.log("--------------- Historique: filterHistoriquesByUndershop STARTED -------------");


    var mysort = { dateCreation: -1 };
    const { page = 1, limit = 5 } = req.query;

    if (req.query.type === "all") {
      if (req.query.day != "" && (req.query.lower != req.query.upper)) {
        console.log("filter by date && pointfid ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "undershop": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "pointFid": { $gte: Number(req.query.lower), $lte: Number(req.query.upper) } }]
        };


      } else if (req.query.day == "" && (req.query.lower != req.query.upper)) {
        console.log("filter by point fid");
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "undershop": { $eq: req.params.id } },
            { "pointFid": { $gte: Number(req.query.lower), $lte: Number(req.query.upper) } }]
        };

      } else if (req.query.day == "" && req.query.lower === req.query.upper) {
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "undershop": { $eq: req.params.id } }]
        };
      }
      else {

        console.log("filter by date ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "type": { $ne: "Transfer" } },
            { "undershop": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } }
          ]
        };
      }

    } else {
      if (req.query.day != "" && (req.query.lower != req.query.upper)) {
        console.log("filter by type && by date && pointfid ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "undershop": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "pointFid": { $gte: req.query.lower, $lte: req.query.upper } },
            { "type": { $eq: req.query.type } }
          ]
        };

      } else if (req.query.day === "" && (req.query.lower != req.query.upper)) {
        console.log("filter by type && by point fid");
        filter = {
          $and: [
            { "undershop": { $eq: req.params.id } },
            { "pointFid": { $gte: req.query.lower, $lte: req.query.upper } },
            { "type": { $eq: req.query.type } }
          ]
        };
      } else if (req.query.day != "") {

        console.log("filter by type && by date ");
        var date = new Date((req.query.day).replace(" ", "+"));
        date.setHours(0, 0, 0, 0);
        var endDate = new Date(date);
        endDate.setHours(23, 59, 59, 59);
        filter = {
          $and: [
            { "undershop": { $eq: req.params.id } },
            { "dateCreation": { $gte: date, $lte: endDate } },
            { "type": { $eq: req.query.type } }
          ]
        };
      } else if (req.query.day === "" && req.query.lower === req.query.upper) {
        filter = {
          $and: [
            { "undershop": { $eq: req.params.id } },
            { "type": { $eq: req.query.type } }
          ]
        };
      }
      else {
        console.log("filter by type ");
        filter =
        {
          "undershop": { $eq: req.params.id }
        }

      }

    }

    const historiques = await Historique.find(filter).sort(mysort).limit(limit * 1).skip((page - 1) * limit).populate("customer").populate("undershop");

    if (historiques === null) {
      throw new Api500Error(`Filter Historique undershop not found`)
    }
    res.status(200).send(historiques);
  } catch (err) {
    next(err)
  }
};


/*-------------- Get TotalFid by date creation and number of days  --------------------------*/
exports.getTotalFidByDate = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "shopkeeper": { $eq: req.params.id } },
              { "type": { $ne: "Transfer" } },
              { "dateCreation": { $gte: old_date } }
            ]
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            total: { $sum: "$pointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "total": 0
            }
            listfinale_stat.push(stat_value)
          }
          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};

/*-------------- Get TotalFid mint and burn by date creation and number of days  --------------------------*/
exports.getTotalFidMintBurnByDate = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidMintBurnByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    //console.log("------", old_date)
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: old_date } },
              { "type": { $ne: "Transfer" } }
            ]
          }
        },
        {
          $project: {
            _id: 0,
            dateCreation: 1,
            PosPointFid: { $cond: [{ $gt: ['$pointFid', 0] }, '$pointFid', 0] },
            NegPointFid: { $cond: [{ $lt: ['$pointFid', 0] }, '$pointFid', 0] }
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            totalPos: { $sum: "$PosPointFid" },
            totalNeg: { $sum: "$NegPointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          //endTime= new Date(endTime.getTime() + dayMillisec)
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "totalPos": 0,
              "totalNeg": 0
            }
            listfinale_stat.push(stat_value)
          }

          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};

function formatDate(date) {
  var d = new Date(date),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2)
    month = '0' + month;
  if (day.length < 2)
    day = '0' + day;

  return [year, month, day].join('-');
}
/*****************stat under shop *************** */
/*-------------- Get TotalFid by date creation and number of days  --------------------------*/
exports.getTotalFidByDate = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: old_date } },
              { "type": { $ne: "Transfer" } }
            ]
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            total: { $sum: "$pointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "total": 0
            }
            listfinale_stat.push(stat_value)
          }
          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};

/*-------------- Get TotalFid mint and burn by date creation and number of days  --------------------------*/
exports.getTotalFidMintBurnByDate = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidMintBurnByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    //console.log("------", old_date)
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "shopkeeper": { $eq: req.params.id } },
              { "dateCreation": { $gte: old_date } },
              { "type": { $ne: "Transfer" } }
            ]
          }
        },
        {
          $project: {
            _id: 0,
            dateCreation: 1,
            PosPointFid: { $cond: [{ $gt: ['$pointFid', 0] }, '$pointFid', 0] },
            NegPointFid: { $cond: [{ $lt: ['$pointFid', 0] }, '$pointFid', 0] }
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            totalPos: { $sum: "$PosPointFid" },
            totalNeg: { $sum: "$NegPointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "totalPos": 0,
              "totalNeg": 0
            }
            listfinale_stat.push(stat_value)
          }
          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};/*-------------- Get TotalFid by date creation and number of days  --------------------------*/
exports.getTotalFidByDateShop = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "id_Store": { $eq: req.params.id } },
              { "dateCreation": { $gte: old_date } },
              { "type": { $ne: "Transfer" } }
            ]
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            total: { $sum: "$pointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "total": 0
            }
            listfinale_stat.push(stat_value)
          }
          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};

/*-------------- Get TotalFid mint and burn by date creation and number of days  --------------------------*/
exports.getTotalFidMintBurnByDateShop = async (req, res, next) => {

  try {
    console.log("--------------- Historique: getTotalFidMintBurnByDate STARTED -------------");
    const { days = 5 } = req.query;
    const old_date = new Date();
    old_date.setDate(old_date.getDate() - days);
    //console.log("------", old_date)
    Historique.aggregate(
      [
        {
          "$match": {
            $and: [
              { "id_Store": { $eq: req.params.id } },
              { "dateCreation": { $gte: old_date } },
              { "type": { $ne: "Transfer" } }
            ]
          }
        },
        {
          $project: {
            _id: 0,
            dateCreation: 1,
            PosPointFid: { $cond: [{ $gt: ['$pointFid', 0] }, '$pointFid', 0] },
            NegPointFid: { $cond: [{ $lt: ['$pointFid', 0] }, '$pointFid', 0] }
          }
        },
        {
          $group: {
            _id: { $dateToString: { format: "%Y-%m-%d", date: "$dateCreation" } },
            totalPos: { $sum: "$PosPointFid" },
            totalNeg: { $sum: "$NegPointFid" }
          }
        },
        { $sort: { "_id": -1 } }

      ]).then(
        result => {
          //console.log(result);
          let initialTime = old_date
            , endTime = new Date()
            , arrTime = []
            , dayMillisec = 24 * 60 * 60 * 1000
            ;
          let listfinale_stat = []
          for (let q = initialTime; q <= endTime; q = new Date(q.getTime() + dayMillisec)) {


            var stat_value = {
              "_id": formatDate(q.toDateString()),
              "totalPos": 0,
              "totalNeg": 0
            }
            listfinale_stat.push(stat_value)
          }
          listfinale_stat = listfinale_stat.concat(result) // merge two arrays
          let foo = new Map();
          for (const tag of listfinale_stat) {
            foo.set(tag._id, tag);
          }
          let final = [...foo.values()]

          res.status(200).send(final);
        })
      .catch(error => {
        throw new Api500Error(error)
      })
  } catch (err) {
    next(err)
  }
};
