const Customer = require('../models/customer.model.js')
const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')
const fidCard= require('../models/fidCard.model.js')

exports.getCustomer = async (req, res, next) => {

  try {
    console.log("--------------- Customer: getCustomer STARTED -------------");
    const customer = await Customer.find().populate('Customer');
    if (customer === null) {
      throw new Api500Error(`Customers not found`)
    }
    res.status(200).send(customer)
  } catch (err) {
    next(err)
  }
}


exports.getCustomerById = async (req, res, next) => {
  try {
    console.log("--------------- Customer: getCustomerById STARTED -------------");
    const customer = await Customer.findById(req.params.id);
    if (customer === null) {
      throw new Api404Error(`Customers with id ${req.params.id} not found.`)
    }
    res.status(200).send(customer)
  } catch (err) {
    next(err)
  }
}

exports.saveCustomer = async (req, res, next) => {

  try {
    console.log("--------------- Customer: saveCustomer STARTED -------------");
    const customer = new Customer({

      firstName: req.body.firstName,
      lastName: req.body.lastName,
      phoneNumber: req.body.phoneNumber,
      gender: req.body.gender,
    })


    const cp = await customer.save()
    if (cp === null) {
      throw new Api404Error(`Customer not saved.`)
    }
    res.status(200).send(cp)

  } catch (err) {
    next(err)
  }
}


exports.deleteCustomer = (req, res, next) => {

  try {
    console.log("--------------- Customer: deleteCustomer STARTED -------------");
    Customer.deleteOne({ _id: req.params.id }).then(
      () => {
        res.status(200).send({
          message: 'Deleted!'
        });
      }
    ).catch(
      (error) => {
        throw new Api404Error(`Customer with id ${req.params.id} not deleted.`)
      }
    );
  } catch (err) {
    next(err)
  }
};


exports.deleteAllCustomer = (req, res, next) => {
  try {
    console.log("--------------- Customer: deleteAllCustomer STARTED -------------");
    Customer.deleteMany({})
      .then(data => {
        res.status(200).send({
          message: `${data.deletedCount} Customers were deleted successfully!`
        });
      })
      .catch(err => {
        throw new Api404Error(`Customers not delted.`)
      });
  } catch (err) {
    next(err)
  }
};

exports.addCustomerRabbitMQ = (customer) => {
  try {
    console.log("--------------- Customer: addCustomerRabbitMQ STARTED -------------");
    if (!customer) {
      throw new Api404Error(`customer Can't be null!.`)
    }

    customer.save().then(data => {
      //console.log("customer Added Successfully!");
    }).catch(err => {
      console.log("Error :", err);

    });
  } catch (err) {
    console.log("Error :", err);
  }
};

exports.updateRabbitMQ = (customer) => {
  try{
    console.log("--------------- Customer: updateRabbitMQ STARTED -------------");
    if (!customer) {
      throw new Api404Error(`customer Can't be null!.`)
    }

  Customer.findByIdAndUpdate(customer.id, {
    firstName: customer.firstName,
    lastName: customer.lastName,
    phoneNumber: customer.phoneNumber,
    imageName: customer.imageName
  }, { new: true })
    .then(data => {
      if (data === null) {
        throw new Api404Error(`Customer with id ${customer.id} not saved.`)
      }
      console.log("customer updated", customer);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        console.log("Shopkeeper not found with id in catch bloc", customer.id);
      }
      console.log("Error updating todo with id", customer.id);
    });
  }catch (err) {
    console.log("Error :", err);
  }
};


exports.deleteRabbitMQ = async (idCustomer)=>{
  const oneCustomer = await Customer.findById(idCustomer).populate(
    {
        path: 'fidelityCards',
       
    });
    for (let index = 0; index < oneCustomer.fidelityCards.length; index++) {
      await fidCard.deleteOne({ _id: oneCustomer.fidelityCards[index]._id })
      }
  Customer.findByIdAndDelete(idCustomer).then(data => {
      if(!data) {
          console.log("Customer not found with id ", idCustomer);
      }
  }).catch(err => {
      if(err.kind === 'ObjectId') {
          console.log("Customer not found with id ", idCustomer);       
      }
      console.log("Error deleting Customer with id", idCustomer);
  });
}