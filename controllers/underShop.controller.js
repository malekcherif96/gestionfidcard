const underShop = require("../models/underShop.js");
const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')

exports.create = (req, res, next) => {
  console.log("--------------- underShop: create STARTED -------------");
  try {
    if (Object.keys(req.body).length === 0) {
      throw new Api500Error(`Content can not be empty!`)
    }
    const under = new underShop({
      id_Shopkeeper: req.body.id_Shopkeeper,
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      role: req.body.role,
      phoneNumber: req.body.phoneNumber,
    });
    // Save Tutorial in the database
    under.save().then(data => {
      res.status(200).send(data);
    }).catch(err => {
      throw new Api500Error(err)
    });
  } catch (err) {
    //res.status(500).send('error',err);
    next(err)
  }
};

exports.addRabbitMQ = (underShop) => {

  try {
    console.log("--------------- underShop: addRabbitMQ STARTED -------------");
    if (!underShop) {
      throw new Api500Error(`RabbitMQ Content can not be empty!`)
    }
   // console.log("UNDER SHOP BEFORE SAVING=", underShop);

    underShop.save().then(data => {
      //console.log("UNDER SHOP Successfully!");
    }).catch(err => {
      console.log("Error :", err);

    });
  } catch (err) {
    //res.status(500).send('error',err);
    res.status(500).send(err)
  }
};




exports.findAll = async (req, res) => {
  try {
    console.log("--------------- underShop: findAll STARTED -------------");
    const under = await underShop.find();
    //res.status(200).json(shopkeeper);
    res.status(200).send(under)
  } catch (err) {
    //res.status(500).send('error',err);
    res.status(500).send(err)
  }
};

exports.findOne = (req, res) => {

  try {
    console.log("--------------- underShop: findOne STARTED -------------");
    underShop.findById(req.params.id)
      .then(data => {
        if (!data) {
          return res.status(404).send({
            message: "shop not found with id " + req.params.id
          });
        }
        res.send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: "shop not found with id " + req.params.id
          });
        }
        return res.status(500).send({
          message: "Error retrieving Card with id " + req.params.id
        });
      });
  } catch (err) {
    //res.status(500).send('error',err);
    res.status(500).send(err)
  }
};

exports.deleteUnderShop = (req, res, next) => {

  try {
    console.log("--------------- underShop: deleteUnderShop STARTED -------------");
    underShop.deleteOne({ _id: req.params.id }).then(
      () => {
        res.status(200).send({
          message: 'Deleted!'
        });
      }
    ).catch(
      (error) => {
        res.status(500).send({
          error: error
        });
      }
    );
  } catch (err) {
    //res.status(500).send('error',err);
    next(err)
  }
};


exports.deleteAll = async (req, res, next) => {
  try {
    console.log("--------------- underShop: deleteAll STARTED -------------");
    const underShop = await underShop.deleteMany();
    res.status(200).send(underShop);
  } catch (err) {
    next(err);
  }
}
exports.findByshop = (req, res, next) => {
  try {
    console.log("--------------- underShop: findByshop STARTED -------------");
    underShop.find().where('id_Shopkeeper').equals(req.params.id_Shopkeeper).then(under => {
      //console.log('all ubder shop', under);
      res.status(200).send(under);
    }).catch(
      (error) => {
        throw new Api500Error(error)
      }
    );

  } catch (err) {
    next(err);
  }
}
