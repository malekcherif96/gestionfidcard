//const truffle_connect = require('../connection/app.js');
//const Web3 = require('web3')
const Customer = require("../models/customer.model.js");
const FidCardPerso = require("../models/fidCardPerso.js");
const fileUploadService = require('../services/upload.service.js');
const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')
///find card by customer id 
exports.findAllByCustomerId = (req, res, next) => {
  try {
    console.log("--------------- FidCardPerso : findAllByCustomerId STARTED -------------");
    FidCardPerso.find().where('customer').equals(req.params.id).populate('Customer')
      .then(data => {
        if (!data) {
          return res.status(404).send({
            message: "Card not found with id " + req.params.id
          });
        }
        res.status(200).send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          return res.status(404).send({
            message: "Card not found with id " + req.params.id
          });
        }
        return res.status(500).send({
          message: "Error retrieving Cards Customer id " + req.params.id
        });
      });
  } catch (err) {
    next(err)
  }
};
// Delete all FidCards from the database.
exports.deleteAll = (req, res, next) => {
  try {
    console.log("--------------- FidCardPerso : deleteAll STARTED -------------");
    FidCardPerso.deleteMany({})
      .then(data => {
        res.status(200).send({
          message: `${data.deletedCount} Tutorials were deleted successfully!`
        });
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while removing all tutorials."
        });
      });
  } catch (err) {
    next(err)
  }
};

//delete card byid 
exports.delete = (req, res, next) => {
  try {
    console.log("--------------- FidCardPerso : delete STARTED -------------");
    FidCardPerso.findByIdAndRemove(req.params.id)
      .then(data => {
        if (!data) {
          return res.status(404).send({
            message: "Todo not found with id " + req.params.id
          });
        }
        res.status(200).send({ message: "Todo deleted successfully!" });
      }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          return res.status(404).send({
            message: "Todo not found with id " + req.params.data
          });
        }
        return res.status(500).send({
          message: "Could not delete todo with id " + req.params.id
        });
      });
  } catch (err) {
    next(err)
  }
};
//find one 
exports.findOne = (req, res, next) => {
  try {
    console.log("--------------- FidCardPerso : findOne STARTED -------------");
  FidCardPerso.findById(req.params.id).populate('Customer')
    .then(data => {
      if (!data) {
        return res.status(404).send({
          message: "Card not found with id " + req.params.id
        });
      }
      res.status(200).send(data);
    }).catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "Card not found with id " + req.params.id
        });
      }
      return res.status(500).send({
        message: "Error retrieving Card with id " + req.params.id
      });
    });
  } catch (err) {
    next(err)
  }
};

// Retrieve all  from the database.
exports.findAll = async (req, res, next) => {
  try {
    console.log("--------------- FidCardPerso : findAll STARTED -------------");
    const fidcard = await FidCardPerso.find().populate('Customer')
    if (data === null) {
      throw new Api404Error(`FidCardPerso not found.`)
    }
    res.status(200).send(fidcard)
  } catch (err) {
    next(err)
  }
};

exports.create = async (req, res, next) => {

  try {
    console.log("--------------- FidCardPerso : create STARTED -------------");
    var fileUrl;
    if (req.files && req.files.file) {
      const file = req.files.file;
      const uploadRes = await fileUploadService.uploadFileToAws(file);
      fileUrl = uploadRes.name;
    } else {
      const errMsg = {
        message: 'FILES_NOT_FOUND',
        messageCode: 'FILES_NOT_FOUND',
        statusCode: 404,
      }
      throw new Api404Error(`FILES_NOT_FOUND FidCardPerso.`)
    }
  

  const aa = JSON.parse(req.body.card);
  console.log("**", aa.dateCreation)
  const fidCardPerso = new FidCardPerso({
    code: aa.code,
    name: aa.type,
    nameShop: aa.nameShop,
    nameCard: aa.nameCard,
    imageCard: fileUrl,
    format: aa.format,
    pass: aa.pass
  });

  Customer.findById(aa.customer.id).then(data => {
    fidCardPerso.customer = data;
   // console.log("image = ", data);
    //console.log("aaaa", fidCardPerso.customer)
    fidCardPerso.save().then(fidCardPerso => {
      Customer.findByIdAndUpdate(fidCardPerso.customer._id,
        { $push: { fidCardPersos: fidCardPerso._id } },
        { new: true, useFindAndModify: false })
        .then(customer => { console.log(customer) });
      return fidCardPerso
    })
      .then(fidCardPerso => {

        res.status(200).send(fidCardPerso);
      })
      .catch(err => {
        throw new Api500Error(err)
      });
  });
} catch (error) {
  next(error);
}
}
