const Shopkeeper = require("../models/Shopkeeper.js");

exports.create = (req, res) => {
    console.log(req.body)
    if (!req.body) {
        res.status(500).send({ message: "Content can not be empty!" });
    }
    const shopkeeper = new Shopkeeper({
        brandName: req.body.brandName,
        logo: req.body.logo,
        shopkeeperId: req.body.shopkeeperId
    });
        // Save Tutorial in the database
    shopkeeper.save().then(data => {
        res.status(200).send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Shopkeeper."
        });
    });
};

exports.addRabbitMQ = (shopkeeper) => {
    if (!shopkeeper) {
        console.log("Shopkeeper Can't be null!")
    }
    console.log("SHOPKEEPER BEFORE SAVING=",shopkeeper);

    shopkeeper.save().then(data => {
        console.log("Shopkeeper Added Successfully!");
    }).catch(err => {
        console.log("Error :",err);

    });
};



exports.updateRabbitMQ = (shopkeeper) => {
    if (!shopkeeper) {
        console.log("Shopkeeper Can't be null!")
    }
    console.log("Shopkeeper updateRabbitMQ", shopkeeper);
      
    Shopkeeper.findByIdAndUpdate(shopkeeper.id, {
        brandName:shopkeeper.brandName,
        logo:shopkeeper.logo,
        is_Activate: shopkeeper.is_Activate,
        tauxRemise: shopkeeper.tauxRemise,
        tauxConversion: shopkeeper.tauxConversion,
        taxIdentificationNumber: shopkeeper.taxIdentificationNumber,
    }, {new: true})
    .then(data => {
        if(!data) {
            console.log("Shopkeeper not found with id in then bloc", shopkeeper.id);
        }
        console.log("Shopkeeper updated", shopkeeper);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            console.log("Shopkeeper not found with id in catch bloc", shopkeeper.id);       
        }
        console.log("Error updating todo with id", shopkeeper.id);
    });
    
};


exports.deleteRabbitMQ = (idShop)=>{
    
    Shopkeeper.findByIdAndDelete(idShop).then(data => {
        if(!data) {
            console.log("Shopkeeper not found with id ", idShop);
        }
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            console.log("Shopkeeper not found with id ", idShop);       
        }
        console.log("Error deleting shopkeeper with id", idShop);
    });
}



exports.findActivateShop = async(req, res) => {
    try{
      const shopkeeper = await Shopkeeper.find().where('activated').equals(true).
      populate({
        path : 'fidelityCards',
            populate : [{    
              path : 'customer',
            }],
      });
      res.status(200).send(shopkeeper)
    }catch(err){

      res.status(500).send('error',err)
    }
  };
  //.where({ "is_Activate": { $eq: 'true' } })
  //.where('is_Activate').equals(false)

  exports.findDeactivateShop = async(req, res) => {
    try{
      const shopkeeper = await Shopkeeper.find().where('activated').equals(false).
      populate({
        path : 'fidelityCards',
            populate : [{    
              path : 'customer',
            }],
      });
      res.status(200).send(shopkeeper)
    }catch(err){

      res.status(500).send('error',err)
    }
  };





exports.findAll = async(req, res) => {
    try{
        console.log('mar7be')
      const shopkeeper = await Shopkeeper.find().populate('fidelityCards');
      //res.status(200).json(shopkeeper);
      res.status(200).send(shopkeeper)
    }catch(err){
      //res.status(500).send('error',err);
      res.status(500).send(err)
    }
  };
  /****************************get number card with id shopkeeper */

  exports.findOne = (req, res) => {

    Shopkeeper.findById(req.params.id)
    .then(data => {
        if(!data) {
            return res.status(404).send({
                message: "shop not found with id " + req.params.id
            });
        }
        res.send(data);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "shop not found with id " + req.params.id
            });
        }
        return res.status(500).send({
            message: "Error retrieving Card with id " + req.params.id
        });
    });
  };
  /****************************** */


  exports.getcarte_numbre = (req, res) => {
    var  nummbrecards=0
    Shopkeeper.findById(req.params.id)
    
    .then(data => {
        if(data) {
            nummbrecards=data.fidelityCards.length;
       
        
        }
        res.json(nummbrecards);
    })
  };
    /****************************** */
exports.deleteAll = async (req, res)=>{
    try{
        const shopkeeper = await Shopkeeper.deleteMany();
        res.status(200).send(shopkeeper);
    }catch(err){
        res.status(500).send('error',err);
    }
}
