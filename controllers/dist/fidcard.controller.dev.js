"use strict";

var FidelityCard = require("../models/fidCard.model.js"); //const truffle_connect = require('../connection/app.js');
//const Web3 = require('web3')


var Shopkeeper = require("../models/Shopkeeper.js"); // exports.geta = (req, res) => {
//   console.log("**** GET /getAccounts ****");
//   truffle_connect.start(function (answer) {
//     console.log(answer);
//     res.status(200).send({
//       success: 'true',
//       accounts:answer
//     });
//   })
// };
// exports.getBal = async (req, res) => {
//   console.log("**** GET /getAccounts ****");
//   const data = await FidelityCard.findById(req.params.id)
//     var b;
//     console.log("**** GET walletAdress ****", data.walletAdress);
//     truffle_connect.getBalance(data.walletAdress,(answer) => {
//     console.log("balance ", answer)
//     b = toString(answer);
//  });
//   res.status(200).send({
//   success: 'true',
//   accounts:data.walletAdress,
//   balance: Web3.utils.fromWei(b)
// });
//  return true;
// };
// burn fidpoint from account of customer


exports.burn = function _callee(req, res) {
  var data, a;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          console.log("**** GET /Burn****");
          console.log("**** ", req.body, "  ****");
          _context.next = 4;
          return regeneratorRuntime.awrap(FidelityCard.findById(req.params.id));

        case 4:
          data = _context.sent;
          a = data.balance - req.body.balance;

          if (!(a > -1)) {
            _context.next = 10;
            break;
          }

          //aa = await truffle_connect.burn(data.walletAdress,req.body.balance,(answer) => {
          //res.status(200).send(answer);
          //console.log(" before an ********", answer);
          FidelityCard.findByIdAndUpdate(req.params.id, {
            code: data.code,
            type: data.type,
            customerId: data.customerId,
            shopkeeper: data.shopkeeper,
            balance: a,
            walletAdress: data.walletAdress
          }, {
            "new": true
          }).then(function (data) {
            if (!data) {
              return res.status(404).send({
                message: "Todo not found with id " + req.params.id
              });
            }

            res.send(data);
          })["catch"](function (err) {
            if (err.kind === 'ObjectId') {
              return res.status(404).send({
                message: "Todo not found with id " + req.params.id
              });
            }

            return res.status(500).send({
              message: "Error updating todo with id " + req.params.id
            });
          }); //res.status(200).send(answer);
          //--------- Blockchaine---------
          //   return answer;
          // });

          _context.next = 11;
          break;

        case 10:
          return _context.abrupt("return", res.status(500).send({
            message: "Balance insuffisant " + req.params.id
          }));

        case 11:
        case "end":
          return _context.stop();
      }
    }
  });
}; //mint pointfid to account of customer


exports.mint = function _callee2(req, res) {
  var data, a;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          // console.log("**** GET /mint  ****");
          console.log("**** ", req.body, "  ****");
          _context2.next = 3;
          return regeneratorRuntime.awrap(FidelityCard.findById(req.params.id));

        case 3:
          data = _context2.sent;
          //aa = await truffle_connect.mint(data.walletAdress,req.body.balance,(answer) => {
          //res.status(200).send(answer);
          //console.log(" before an ********", answer);
          a = data.balance + req.body.balance;
          FidelityCard.findByIdAndUpdate(req.params.id, {
            code: data.code,
            type: data.type,
            customerId: data.customerId,
            shopkeeper: data.shopkeeper,
            balance: a,
            walletAdress: data.walletAdress
          }, {
            "new": true
          }).then(function (data) {
            if (!data) {
              return res.status(404).send({
                message: "Todo not found with id " + req.params.id
              });
            }

            res.send(data);
          }).then(function (data) {})["catch"](function (err) {
            if (err.kind === 'ObjectId') {
              return res.status(404).send({
                message: "Todo not found with id " + req.params.id
              });
            }

            return res.status(500).send({
              message: "Error updating todo with id " + req.params.id
            });
          }); //res.status(200).send(answer);
          //-------Blockchaine---------
          //   return answer;
          // });

          return _context2.abrupt("return", res);

        case 7:
        case "end":
          return _context2.stop();
      }
    }
  });
}; // Create and Save a new Tuseltorial


exports.create = function (req, res) {
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  } //---------create wallet----------
  // var acc;
  // truffle_connect.createWallet((answer) => {
  //   console.log("Wallet added : ",answer);
  //   acc = answer
  // ---------------------------------------


  var fidelityCard = new FidelityCard({
    code: req.body.code,
    type: req.body.type,
    customerId: req.body.customerId,
    balance: 0,
    walletAdress: null
  });
  console.log("Shopkeeper ID =", req.body.shopkeeper.id);
  Shopkeeper.findOne({
    '_id': req.body.shopkeeper.id
  }).then(function (data) {
    fidelityCard.shopkeeper = data;
    console.log(fidelityCard.shopkeeper);
    fidelityCard.save().then(function (fidelityCard) {
      Shopkeeper.findByIdAndUpdate(fidelityCard.shopkeeper._id, {
        $push: {
          fidelityCards: fidelityCard._id
        }
      }, {
        "new": true,
        useFindAndModify: false
      }).then(function (shopkeeper) {
        console.log(shopkeeper);
      });
      return fidelityCard;
    }).then(function (fidelityCard) {
      //console.log("ok =",fidelityCard);
      res.status(200).send(fidelityCard);
    })["catch"](function (err) {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Tutorial."
      });
    });
  }); // Save Tutorial in the database
  //});
}; // Retrieve all  from the database.


exports.findAll = function _callee3(req, res) {
  var fidcard;
  return regeneratorRuntime.async(function _callee3$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          _context3.prev = 0;
          _context3.next = 3;
          return regeneratorRuntime.awrap(FidelityCard.find().populate('shopkeeper'));

        case 3:
          fidcard = _context3.sent;
          res.status(200).json(fidcard);
          _context3.next = 10;
          break;

        case 7:
          _context3.prev = 7;
          _context3.t0 = _context3["catch"](0);
          res.status(status).send('error', _context3.t0);

        case 10:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[0, 7]]);
}; // Find a single  with an id
// Find a single todo with a id


exports.findOne = function (req, res) {
  FidelityCard.findById(req.params.id).then(function (data) {
    if (!data) {
      return res.status(404).send({
        message: "Card not found with id " + req.params.id
      });
    }

    res.send(data);
  })["catch"](function (err) {
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "Card not found with id " + req.params.id
      });
    }

    return res.status(500).send({
      message: "Error retrieving Card with id " + req.params.id
    });
  });
}; // Update a  by the id in the request  


exports.update = function (req, res) {
  // Validate Request
  if (!req.body) {
    return res.status(400).send({
      message: "Todo can not be empty"
    });
  } // Find todo and update it with the request body


  FidelityCard.findByIdAndUpdate(req.params.id, {
    code: req.body.code,
    type: req.body.type,
    customerId: req.body.customerId,
    shopkeeper: req.body.shopkeeper,
    balance: req.body.balance,
    walletAdress: req.body.walletAdress
  }, {
    "new": true
  }).then(function (data) {
    if (!data) {
      return res.status(404).send({
        message: "Todo not found with id " + req.params.id
      });
    }

    res.send(data);
  })["catch"](function (err) {
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "Todo not found with id " + req.params.id
      });
    }

    return res.status(500).send({
      message: "Error updating todo with id " + req.params.id
    });
  });
}; // Delete a  with the specified id in the request
// Delete a todo with the specified id in the request


exports["delete"] = function (req, res) {
  FidelityCard.findByIdAndRemove(req.params.id).then(function (data) {
    if (!data) {
      return res.status(404).send({
        message: "Todo not found with id " + req.params.id
      });
    }

    res.send({
      message: "Todo deleted successfully!"
    });
  })["catch"](function (err) {
    if (err.kind === 'ObjectId' || err.name === 'NotFound') {
      return res.status(404).send({
        message: "Todo not found with id " + req.params.data
      });
    }

    return res.status(500).send({
      message: "Could not delete todo with id " + req.params.id
    });
  });
}; // Delete all FidCards from the database.


exports.deleteAll = function (req, res) {
  FidelityCard.deleteMany({}).then(function (data) {
    res.send({
      message: "".concat(data.deletedCount, " Tutorials were deleted successfully!")
    });
  })["catch"](function (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while removing all tutorials."
    });
  });
};

exports.findAllByCustomerId = function (req, res) {
  console.log(req.params.id);
  FidelityCard.find().where('customerId').equals(req.params.id).populate('shopkeeper').then(function (data) {
    if (!data) {
      return res.status(404).send({
        message: "Card not found with id " + req.params.id
      });
    }

    res.status(200).send(data);
  })["catch"](function (err) {
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "Card not found with id " + req.params.id
      });
    }

    return res.status(500).send({
      message: "Error retrieving Cards Customer id " + req.params.id
    });
  });
};

exports.findByCode = function (req, res) {
  // FindByCode
  FidelityCard.find().where('code').equals(req.params.code).then(function (data) {
    console.log(data);
    res.status(200).send(data);
  }); // Save FidCard in the database
};