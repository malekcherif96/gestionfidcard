"use strict";

var Shopkeeper = require("../models/Shopkeeper.js");

exports.create = function (req, res) {
  console.log(req.body);

  if (!req.body) {
    res.status(500).send({
      message: "Content can not be empty!"
    });
  }

  var shopkeeper = new Shopkeeper({
    brandName: req.body.brandName,
    logo: req.body.logo,
    shopkeeperId: req.body.shopkeeperId
  }); // Save Tutorial in the database

  shopkeeper.save().then(function (data) {
    res.status(200).send(data);
  })["catch"](function (err) {
    res.status(500).send({
      message: err.message || "Some error occurred while creating the Shopkeeper."
    });
  });
};

exports.addRabbitMQ = function (shopkeeper) {
  if (!shopkeeper) {
    console.log("Shopkeeper Can't be null!");
  }

  console.log("SHOPKEEPER BEFORE SAVING=", shopkeeper);
  shopkeeper.save().then(function (data) {
    console.log("Shopkeeper Added Successfully!");
  })["catch"](function (err) {
    console.log("Error :", err);
  });
};

exports.findAll = function _callee(req, res) {
  var shopkeeper;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.prev = 0;
          _context.next = 3;
          return regeneratorRuntime.awrap(Shopkeeper.find().populate('fidelityCards'));

        case 3:
          shopkeeper = _context.sent;
          res.status(200).json(shopkeeper);
          _context.next = 10;
          break;

        case 7:
          _context.prev = 7;
          _context.t0 = _context["catch"](0);
          res.status(500).send('error', _context.t0);

        case 10:
        case "end":
          return _context.stop();
      }
    }
  }, null, null, [[0, 7]]);
};

exports.deleteAll = function _callee2(req, res) {
  var shopkeeper;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.prev = 0;
          _context2.next = 3;
          return regeneratorRuntime.awrap(Shopkeeper.deleteMany());

        case 3:
          shopkeeper = _context2.sent;
          res.status(200).json(shopkeeper);
          _context2.next = 10;
          break;

        case 7:
          _context2.prev = 7;
          _context2.t0 = _context2["catch"](0);
          res.status(500).send('error', _context2.t0);

        case 10:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[0, 7]]);
};