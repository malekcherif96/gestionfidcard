const FidelityCard = require("../models/fidCard.model.js");
//const truffle_connect = require('../connection/app.js');
//const Web3 = require('web3')
const Shopkeeper = require("../models/Shopkeeper.js");
const Historique = require("../models/historique.model.js");
const Customer = require("../models/customer.model.js");
const UnderShop = require("../models/underShop.js");
const AccordFidcard = require("../models/accordFidcard");
const AccordFidcardService = require('../services/accordFidcard.service');
const Api404Error = require('../Handler/api404Error')
const Api500Error = require('../Handler/api500Error')
const Api600Error = require('../Handler/api600Error');
const Api700Error = require('../Handler/api700Error');
const customerModel = require("../models/customer.model.js");
const { listenerCount } = require("../models/fidCard.model.js");
const { ListIndexesCursor } = require("mongodb");
// exports.geta = (req, res) => {
//   console.log("**** GET /getAccounts ****");
//   truffle_connect.start(function (answer) {
//     console.log(answer);
//     res.status(200).send({
//       success: 'true',
//       accounts:answer
//     });
//   })
// };
// exports.getBal = async (req, res) => {
//   console.log("**** GET /getAccounts ****");

//   const data = await FidelityCard.findById(req.params.id)
//     var b;
//     console.log("**** GET walletAdress ****", data.walletAdress);
//     truffle_connect.getBalance(data.walletAdress,(answer) => {
//     console.log("balance ", answer)
//     b = toString(answer);

//  });
//   res.status(200).send({
//   success: 'true',
//   accounts:data.walletAdress,
//   balance: Web3.utils.fromWei(b)
// });
//  return true;
// };


// burn fidpoint from account of customer
exports.burn = async (req, res, next) => {
  console.log("--------------- FidCard: burn STARTED -------------");
  let card = {};
  var testUnde = "";
  var store = "";
  var a;
  try {
    if (Object.keys(req.body).length === 0) {
      throw new Api500Error(`Content can not be empty!`)
    }
    let x = await FidelityCard.findById(req.params.id)
    if (x === null) {
      throw new Api404Error(`FidelityCard with id ${req.params.id} not found`)
    }

    card = x;
    a = card.balance - req.body.balance;
    if (a > -1) {
      //aa = await truffle_connect.burn(card.walletAdress,req.body.balance,(answer) => {
      if (req.body.id_under != "") {
        let under = await UnderShop.findById(req.body.id_under)
        if (under === null) {
          throw new Api404Error(`UnderShop with id ${req.body.id_under} not found: Your are not allowed`)
        }
        testUnde = under._id
        store = under.id_Store
      }

      const histori = new Historique({
        type: req.body.type,
        dateCreation: new Date(),
        pointFid: (-1) * req.body.balance,
        fidcard: card,
        shopkeeper: card.shopkeeper,
        customer: card.customer,
        undershop: testUnde,
        id_Store: store
      });

      let historique = await histori.save()

      if (historique === null) {
        throw new Api500Error(`Transaction historique is not generated.`)
      }

      let fidcard = await FidelityCard.findByIdAndUpdate(req.params.id,
        { balance: a },
        { new: true });

      if (fidcard === null) {
        throw new Api500Error(`Fidcard did not updated.`)
      }

      process.on('unhandledRejection', error => {
        console.log(error)
        // process.exit(1);
      })
      process.on('uncaughtException', error => {
        console.log(error)
      })

      return res.status(200).send(fidcard);
      //-------Blockchaine---------
      //   return answer;
      // });
    } else {
      //return res.status(200).send("Balance insuffisant");
      throw new Api600Error(`INSUFFICIENT_BALANCE`)
    }
  } catch (error) {
    next(error)
  }
  //return res;
};

//mint pointfid to account of customer
exports.mint = async (req, res, next) => {
  console.log("--------------- FidCard: mint STARTED -------------");
  let card = {};
  var testUnde = "";
  var store = "";
  var a;
  try {
    if (Object.keys(req.body).length === 0) {
      throw new Api500Error(`Content can not be empty!`)
    }
    let fidCard = await FidelityCard.findById(req.params.id)

    if (fidCard === null) {
      throw new Api404Error(`FidelityCard with id ${req.params.id} not found`)
    }

    card = fidCard;
    a = card.balance + req.body.balance;
    //aa = await truffle_connect.mint(card.walletAdress,req.body.balance,(answer) => {
    if (req.body.id_under != "") {
      let under = await UnderShop.findById(req.body.id_under)
      if (under === null) {
        throw new Api404Error(`UnderShop with id ${req.body.id_under} not found: Your are not allowed`)
      }
      testUnde = under._id
      store = under.id_Store
    }
    let shopkeeperSource;
    let customerSource;
    let fidcardSourceVar;
    console.log("fidcardSource", req.body.fidcardSource)
    req.body.shopkeeperSource ? shopkeeperSource = req.body.shopkeeperSource : shopkeeperSource = null;
    req.body.customerSource ? customerSource = req.body.customerSource : customerSource = null;
    req.body.fidcardSource ?
      fidcardSourceVar = await FidelityCard.findById(req.body.fidcardSource.id) : fidcardSourceVar = null;
    //let test = JSON.parse(req.body.fidcardSource)
    //console.log("test",test)
    if(fidcardSourceVar && fidcardSourceVar.balance < req.body.balance){
      throw new Api600Error(`INSUFFICIENT_BALANCE`)
    }

    if(shopkeeperSource && shopkeeperSource != card.shopkeeper){
      let accordFidcard = AccordFidcardService.getAccordFidcardByShops(shopkeeperSource, card.shopkeeper);
      if(accordFidcard == null){
        throw new Api700Error(`ACCORDFIDCARD_NOT_FOUND`)
      }
    }

    console.log("fidcardSourceVar", fidcardSourceVar)
    const histori = new Historique({
      type: req.body.type,
      dateCreation: new Date(),
      pointFid: req.body.balance,
      fidcard: card,
      shopkeeper: card.shopkeeper,
      customer: card.customer,
      undershop: testUnde,
      id_Store: store
    });

    let historique = await histori.save()

    if (historique === null) {
      throw new Api500Error(`Transaction historique is not generated.`)
    }

    let fidcard = await FidelityCard.findByIdAndUpdate(req.params.id,
      { balance: a },
      { new: true });
    console.log("FIDCARD = ", fidcard);

    if (fidcard === null) {
      throw new Api500Error(`Fidcard did not updated.`)
    }

    /********* make historique transaction if tranfert client to client */
    if (fidcardSourceVar) {
      const histori2 = new Historique({
        type: req.body.type,
        dateCreation: new Date(),
        pointFid: (-1) * req.body.balance,
        fidcard: fidcardSourceVar,
        shopkeeper: fidcardSourceVar.shopkeeper,
        customer: fidcardSourceVar.customer,
        undershop: testUnde,
        id_Store: store,
        shopkeeperSource: card.shopkeeper,
        customerSource: card.customer,
        fidcardSourceVar: fidcard
      });

      let historique2 = await histori2.save()
      if (historique2 === null) {
        throw new Api500Error(`Transaction historique 2 is not generated.`)
      }

      a2 = fidcardSourceVar.balance + ((-1) * req.body.balance);
      let fidcardSourceUpdated = await FidelityCard.findByIdAndUpdate(fidcardSourceVar.id,
        { balance: a2 },
        { new: true , useFindAndModify: false });
      console.log("FIDCARD = ", fidcard);
      console.log("FIDCARD 2 = ", fidcardSourceUpdated);
      if (fidcardSourceUpdated === null) {
        throw new Api500Error(`fidcard Source for transfert did not updated.`)
      }
    }

    process.on('unhandledRejection', error => {
      console.log(error)
    })
    process.on('uncaughtException', error => {
      console.log(error)
    })

    return res.status(200).send(fidcard);
    //-------Blockchaine---------
    //   return answer;
    // });

  } catch (error) {
    next(error)
  }
};



exports.create = async (req, res, next) => {
  console.log("--------------- FidCard : create STARTED -------------");
  try {
    if (Object.keys(req.body).length === 0) {
      throw new Api500Error(`Content can not be empty!`)
    }
    //---------create wallet----------

    // var acc;
    // truffle_connect.createWallet((answer) => {
    //   console.log("Wallet added : ",answer);
    //   acc = answer

    // ---------------------------------------

    const fidelityCard = new FidelityCard({
      code: req.body.code,
      type: req.body.type,
      //customer: req.body.customerId,
      balance: 0,
      walletAdress: null,
    });

    let shop = await Shopkeeper.findById(req.body.shopkeeper.id)
    if (shop === null) {
      throw new Api404Error(`Shopkeeper with id ${req.body.shopkeeper.id} not found.`)
    }
    fidelityCard.shopkeeper = shop

    let customer = await Customer.findById(req.body.customer.id)

    if (customer === null) {
      throw new Api404Error(`Customer with id ${req.body.customer.id} not found.`)
    }
    fidelityCard.customer = customer;

    let newCard = await fidelityCard.save();
    if (newCard === null) {
      throw new Api500Error(`fidelityCard is not saving.`)
    }
    let uShop = await Shopkeeper.findByIdAndUpdate(newCard.shopkeeper._id,
      { $push: { fidelityCards: newCard._id } },
      { new: true, useFindAndModify: false })

    if (uShop === null) {
      throw new Api500Error(`Shopkeeper is not updated.`)
    }

    let uCustomer = await Customer.findByIdAndUpdate(newCard.customer._id,
      { $push: { fidelityCards: newCard._id } },
      { new: true, useFindAndModify: false })

    if (uCustomer === null) {
      throw new Api500Error(`Customer is not updated.`)
    }

    process.on('unhandledRejection', error => {
      console.log(error)
      // process.exit(1);
    })
    process.on('uncaughtException', error => {
      console.log(error)
    })

    return res.status(200).send(newCard);
  } catch (error) {
    next(error)
  }
};



// Retrieve all  from the database.
exports.findAll = async (req, res, next) => {
  console.log("--------------- FidCard : create STARTED -------------");
  try {
    const fidcard = await FidelityCard.find().populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    })
    res.status(200).send(fidcard)
  } catch (err) {
    res.status(500).send(err)
    next(err)
  }
};

exports.findOne = (req, res, next) => {
  try {
    console.log("--------------- FidCard : findOne STARTED -------------");
    FidelityCard.findById(req.params.id)
      .then(data => {
        if (data === null) {
          throw new Api404Error(`FidelityCard with id ${req.params.id} not found.`)
        }
        res.send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          throw new Api404Error(`FidelityCard with ObjectId ${req.params.id} not found.`)
        }
        throw new Api500Error(err)
      });
  } catch (err) {
    next(err)
  }
};


// Update a  by the id in the request  
exports.update = (req, res, next) => {
  // Validate Request
  try {
    console.log("--------------- FidCard : update STARTED -------------");
    if (Object.keys(req.body).length === 0) {
      throw new Api500Error(`Content can not be empty!`)
    }

    // Find todo and update it with the request body
    FidelityCard.findByIdAndUpdate(req.params.id, {
      code: req.body.code,
      type: req.body.type,
      customer: req.body.customer,
      shopkeeper: req.body.shopkeeper,
      balance: req.body.balance,
      walletAdress: req.body.walletAdress,
    }, { new: true })
      .then(data => {
        if (data === null) {
          throw new Api500Error(`FidelityCard with id ${req.params.id} not updated.`)
        }
        res.send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          throw new Api404Error(`FidelityCard with id ${req.params.id} not updated.`)
        }
        throw new Api500Error(err)
      });
    process.on('unhandledRejection', error => {
      console.log(error)
      // process.exit(1);
    })
    process.on('uncaughtException', error => {
      console.log(error)
    })

  } catch (err) {
    next(err)
  }
};

// Delete a  with the specified id in the request
// Delete a todo with the specified id in the request
exports.delete = (req, res, next) => {
  try {
    console.log("--------------- FidCard : delete STARTED -------------");
    FidelityCard.findByIdAndRemove(req.params.id)
      .then(data => {
        if (data === null) {
          throw new Api500Error(`FidelityCard with id ${req.params.id} not deleted.`)
        }
        res.send({ message: "FidelityCard deleted successfully!" });
      }).catch(err => {
        if (err.kind === 'ObjectId' || err.name === 'NotFound') {
          throw new Api404Error(`FidelityCard with ObjectId ${req.params.id} not deleted.`)
        }
        throw new Api500Error(err)
      });
  } catch (err) {
    next(err)
  }
};

// Delete all FidCards from the database.
exports.deleteAll = (req, res, next) => {
  try {
    console.log("--------------- FidCard : deleteAll STARTED -------------");
    FidelityCard.deleteMany({})
      .then(data => {
        res.send({
          message: `${data.deletedCount} FidelityCard were deleted successfully!`
        });
      })
      .catch(err => {
        throw new Api500Error(err)
      });
  } catch (err) {
    next(err)
  }
};

exports.findAllByCustomerId = (req, res, next) => {
  try {
    console.log("--------------- FidCard : findAllByCustomerId STARTED -------------");
    FidelityCard.find().where('customer').equals(req.params.id).populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    })
      .then(data => {
        console.log('data fidelitycard',data)
        if (data === null) {
          throw new Api404Error(`FidelityCard of customer with id ${req.params.id} not found.`)
        }
        res.status(200).send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          throw new Api404Error(`FidelityCard of customer with ObjectId ${req.params.id} not found.`)
        }
        throw new Api500Error(err) 
      });
  } catch (err) {
    next(err)
  }
};

exports.findFidCrdsByShopkeeper = async (req, res, next) => {
  try {
    console.log("--------------- FidCard : findFidCrdsByShopkeeper STARTED -------------");
    FidelityCard.find().where('shopkeeper').equals(req.params.id).populate('customer')
      .then(data => {
        if (data === null) {
          throw new Api404Error(`FidelityCard of shopkeeper with id ${req.params.id} not found.`)
        }
        res.status(200).send(data);
      }).catch(err => {
        if (err.kind === 'ObjectId') {
          throw new Api404Error(`FidelityCard of shopkeeper with ObjectId ${req.params.id} not found.`)
        }
        throw new Api500Error(err)
      });
  } catch (err) {
    next(err)
  }
}

exports.findFidCardsByShopkeeperPaginated = async (req, res, next) => {
  console.log("----- API findFidCardsByShopkeeperPaginated STARTED -----")
  console.log(
    "---- PAGE: ", req.query.page,
    " LIMIT: ", req.query.limit,
    " SHOP ID: ", req.query.shopId,
    " QUERY:", req.query.searchQuery,
    " SORTFIELD: ", req.query.sortField, "SORTORDER: ", req.query.sortOrder);
  try {
    const page = parseInt(req.query.page);
    const limit = parseInt(req.query.limit);
    const sortField = String(req.query.sortField);
    const sortOrder = parseInt(req.query.sortOrder);
    var sort = {}
    sort[sortField] = sortOrder;
    FidelityCard.aggregate([
      {
        $lookup: {
          "from": "customers",
          "localField": "customer",
          "foreignField": "_id",
          "as": "listeCustomer",
        }
      },
      { $unwind: "$listeCustomer" },

      {
        $match: {
          $and: [
            { "shopkeeper": { $eq: req.query.shopId } },
            {
              $or: [
                { "listeCustomer.phoneNumber": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
                { "listeCustomer.firstName": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
                { "listeCustomer.lastName": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
              ]
            }
          ]
        },
      },
      {
        $project: {
          _id: 1,
          code: 1,
          type: 1,
          balance: 1,
          shopkeeper: 1,
          customer: "$listeCustomer._id",
          firstName: "$listeCustomer.firstName",
          lastName: "$listeCustomer.lastName",
          phoneNumber: "$listeCustomer.phoneNumber",
          gender: "$listeCustomer.gender",
          createdAt: 1
        }
      },
      { $sort: sort },
      {
        $facet: {
          metadata: [{ $count: "total" }, { $addFields: { page: page } }],
          data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
        }
      },

    ]).then(data => {

      res.status(200).send(data[0]);
    })
  } catch (err) {
    next(err)
  }
}

exports.findUserForAdminPaginated = async (req, res, next) => {
  console.log("----- API findUserForAdminPaginated STARTED -----")
  console.log(
    "---- PAGE: ", req.query.page,
    " LIMIT: ", req.query.limit,   
    " QUERY:", req.query.searchQuery,
    " SORTFIELD: ", req.query.sortField, "SORTORDER: ", req.query.sortOrder);
    if (req.query.searchQuery !=''){
      try {
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        const sortField = String(req.query.sortField);
        const sortOrder = parseInt(req.query.sortOrder);
        var sort = {}
        sort[sortField] = sortOrder;
       
        Customer.aggregate([
         
    
          {
            $match: {
             
          
               
                  $or: [
                    { "phoneNumber": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
                    { "firstName": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
                    { "lastName": { $regex: new RegExp('.*' + req.query.searchQuery + '.*', "i") } },
                  ]
                
            
            },
          },
        
          { $sort: sort },
          {
            $facet: {
              metadata: [{ $count: "total" }, { $addFields: { page: page } }],
              data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
            }
          },
    
        ]).then(data => {
    
          res.status(200).send(data[0]);
        })
      } catch (err) {
        next(err)
      }
    }else{
      try {
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        const sortField = String(req.query.sortField);
        const sortOrder = parseInt(req.query.sortOrder);
        var sort = {}
        sort[sortField] = sortOrder;
       
        Customer.aggregate([
        
          { $sort: sort },
          {
            $facet: {
              metadata: [{ $count: "total" }, { $addFields: { page: page } }],
              data: [{ $skip: ((page - 1) * limit) }, { $limit: limit }]
            }
          },
    
        ]).then(data => {
    
          res.status(200).send(data[0]);
        })
      } catch (err) {
        next(err)
      }
    }
  
}
exports.findByCode = async (req, res, next) => {

  // FindByCode
  try {
    console.log("--------------- FidCard : findByCode STARTED -------------");
    FidelityCard.findOne().where('code').equals(req.params.code).populate('customer').populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    }).then(data => {
      console.log(data);
      var notfound = 0
      if (data) {
        res.status(200).send(data);

      } else {
        res.json(notfound)
      }
    }).catch(err => {
      console.log(err)
    });
  } catch (err) {
    next(err)
  }

  // Save FidCard in the database
}

exports.sendPointFidNewOrder = async (req) => {
  try {
    //req={ idCustomer:order.customer , shopkeeper:order.shopkeeper, total:order.amount}
    console.log(req)
    let code = req.shopkeeper + '' + req.idCustomer;
    let card = await FidelityCard.findOne({ 'code': { $eq: code } }).populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    })
    console.log("card : ", card);

    if (card == null) {
      let fidelityCard = new FidelityCard({
        code: code,
        type: true,
        //customer: req.body.customerId,
        balance: 0,
        walletAdress: null,
      });
      fidelityCard.shopkeeper = await Shopkeeper.findOne({ '_id': req.shopkeeper })
      fidelityCard.customer = await Customer.findOne({ '_id': req.idCustomer })

      card = await fidelityCard.save();
      if (card === null) {
        throw new Api500Error(`fidelityCard is not saving.`)
      }
      let uShop = await Shopkeeper.findByIdAndUpdate(card.shopkeeper._id,
        { $push: { fidelityCards: card._id } },
        { new: true, useFindAndModify: false })

      if (uShop === null) {
        throw new Api500Error(`Shopkeeper is not updated.`)
      }

      let uCustomer = await Customer.findByIdAndUpdate(card.customer._id,
        { $push: { fidelityCards: card._id } },
        { new: true, useFindAndModify: false })

      if (uCustomer === null) {
        throw new Api500Error(`Customer is not updated.`)
      }
      console.log("new card", card)
    }
    //---------------------------- Mint point Fid -----------------------
    console.log(card)
    if (card) {
      let pointFid = 0;
      if (req.mintFid > 0)
        pointFid = req.mintFid;
      /* else
        pointFid = (req.total * card.shopkeeper.tauxRemise) / card.shopkeeper.tauxConversion; */
      console.log("pointFid :", pointFid)
      //let mintResult = await this.mint(newReq)
      const histori = new Historique({
        type: "Order",
        dateCreation: new Date(),
        pointFid: pointFid,
        fidcard: card,
        shopkeeper: card.shopkeeper,
        customer: card.customer,
        undershop: "",
        id_Store: ""
      });

      let historique = await histori.save()

      if (historique === null) {
        throw new Api500Error(`Transaction historique is not generated.`)
      }
      console
      let fidcard = await FidelityCard.updateOne({ _id: card._id },
        { $inc: { balance: pointFid } })


      console.log("FIDCARD = ", fidcard);

    }
    //let x=await 
  } catch (error) {
    console.log(error)
  }
}

exports.burnFidProcessingOrder = async  (req, res, next) => {
  try {
    //req={ idCustomer:order.customer , shopkeeper:order.shopkeeper, total:order.amount}
    console.log(req)
    let code = req.body.shopkeeper + '' + req.body.idCustomer;
    let card = await FidelityCard.findOne({ 'code': { $eq: code } }).populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    })
    console.log("card : ", card);
    if (card) {
      let pointFid = 0;
     
      pointFid = card.balance -(req.body.burnFid);
      
      const histori = new Historique({
        type: "Processing",
        orderId:req.body.orderId,
        dateCreation: new Date(),
        pointFid: (-1) *req.body.burnFid,
        fidcard: card,
        shopkeeper: card.shopkeeper,
        customer: card.customer,
        undershop: "",
        id_Store: ""
      });

      let historique = await histori.save()

      if (historique === null) {
        throw new Api500Error(`Transaction historique is not generated.`)
      }

      let fidcard = await FidelityCard.updateOne({ _id: card._id },
        { balance: pointFid},)


     
        res.status(200).send(fidcard);
    }
    //let x=await 
  } catch (error) {
    next(error)
  }
}

exports.burnFidCanceledOrder = async  (req, res, next) => {
  try {
   
    console.log(req)
    let code = req.body.shopkeeper + '' + req.body.idCustomer;
    let oneHistory = await Historique.findOne({ $and: [{ 'orderId': { $eq: req.body.orderId } },{ 'type': { $eq: 'Processing' } }]})

    let card = await FidelityCard.findOne({ 'code': { $eq: code } }).populate({
      path: 'shopkeeper',
      select: '_id brandName logo is_Activate  tauxConversion tauxRemise taxIdentificationNumber ',
    })
    console.log("card : ", card);
    if(oneHistory){
    if (card) {
      let pointFid = 0;
     
      pointFid = card.balance +(req.body.burnFid);
      
      const histori = new Historique({
        type: "Cancelled",
        orderId:req.body.orderId,
        dateCreation: new Date(),
        pointFid: req.body.burnFid,
        fidcard: card,
        shopkeeper: card.shopkeeper,
        customer: card.customer,
        undershop: "",
        id_Store: ""
      });

      let historique = await histori.save()

      if (historique === null) {
        throw new Api500Error(`Transaction historique is not generated.`)
      }

      let fidcard = await FidelityCard.updateOne({ _id: card._id },
        { balance: pointFid},)


     
        res.status(200).send(fidcard);
    }}
    //let x=await 
  } catch (error) {
    next(error)
  }
}

exports.burnFidComplitedOrder = async  (req, res, next) => {
  try {
   
    let oneHistory = await Historique.findOne({ $and: [{ 'orderId': { $eq: req.body.orderId } },{ 'type': { $eq: 'Processing' } }]})

   
    if (oneHistory) {
    
      histo =await Historique.findByIdAndUpdate(oneHistory._id,
         { type: 'Order',
         dateCreation: new Date(), } ,
        { new: true, useFindAndModify: false })

    


     
        res.status(200).send(histo);
    }
    //let x=await 
  } catch (error) {
    next(error)
  }
}
exports.findFidCardsByCustomerAndShopkeeper = async (req, res, next) => {
  console.log("----- findFidCardByCustomerAndShopkeeper -----");
  const idCustomer=req.query.idCustomer;
  let idShops = req.query.idShops;
  idShops = idShops.split(',');

  filter = {
    customer : { $eq : idCustomer},
    shopkeeper : { $in : idShops }
  }

  try{
    const cards = await FidelityCard.find(filter).populate("shopkeeper");
    res.status(200).send(cards);
  }catch(err){
    next(err);
  }
}
exports.countfid = async (req, res, next) => {
  
  try{
    const cards = await FidelityCard.countDocuments()
    console.log(cards)
    res.send(cards);
  }catch(err){
    next(err);
  }
}
